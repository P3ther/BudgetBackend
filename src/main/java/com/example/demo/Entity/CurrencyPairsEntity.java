package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrencyPairsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @JoinColumn(name = "firstCurrency")
    private CurrencyEntity firstCurrency;
    @ManyToOne()
    @JoinColumn(name = "secondCurrency")
    private CurrencyEntity secondCurrency;
    private BigDecimal exchangeRate;
    private LocalDateTime lastChange;
}
