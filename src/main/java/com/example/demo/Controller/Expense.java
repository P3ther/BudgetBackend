package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.ExpenseInterface;
import com.example.demo.Dto.BillDto;
import com.example.demo.Dto.EkasaResponseDto.EkasaResponseDto;
import com.example.demo.Dto.ExpenseDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.ExpenseMapper;
import com.example.demo.Services.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Expense implements ExpenseInterface {
    private final ExpenseService expenseService;
    private final ExpenseMapper expenseMapper;

    private final UserHelper userHelper;

    @Autowired
    public Expense(ExpenseService expenseService, ExpenseMapper expenseMapper, UserHelper userHelper) {
        this.expenseService = expenseService;
        this.expenseMapper = expenseMapper;
        this.userHelper = userHelper;
    }

    public ExpenseDto getExpense(Long id, String token) throws EntityNotFoundException {
        return expenseMapper.convertToDto(expenseService.getExpense(id, userHelper.getUser(token)));
    }

    public List<ExpenseDto> expensesByTime(StartEndTimeDto startEndTimeDto, String token) {
        return  expenseMapper.convertToListDto(
                expenseService.expensesByTime(startEndTimeDto, userHelper.getUser(token))
        );
    }

    public List<ExpenseDto> expensesByTimeAndCategory(Long categoryId, StartEndTimeDto startEndTimeDto, String token) {
        return  expenseMapper.convertToListDto(
                expenseService.expensesByTimeAndCategory(categoryId, startEndTimeDto, userHelper.getUser(token))
        );
    }

    public ExpenseDto addExpense(ExpenseDto expenseDto, String token) throws EntityNotFoundException {
        return expenseMapper.convertToDto(
                expenseService.addExpense(
                        expenseMapper.convertToEntity(expenseDto), userHelper.getUser(token)
                ));
    }

    public List<ExpenseDto> addMultipleExpenses(List<ExpenseDto> expenseDtoList, String token) throws EntityNotFoundException {
        return expenseMapper.convertToListDto(
                expenseService.addExpenses(expenseMapper.convertToListEntity(expenseDtoList), userHelper.getUser(token))
        );
    }

    public ExpenseDto updateExpense(ExpenseDto expenseDto, String token) throws EntityNotFoundException {
        return expenseMapper.convertToDto(
                expenseService.updateExpense(expenseMapper.convertToEntity(expenseDto), userHelper.getUser(token))
        );
    }

    public void deleteExpense(ExpenseDto expenseDto, String token) throws EntityNotFoundException {
        expenseService.deleteExpense(expenseMapper.convertToEntity(expenseDto), userHelper.getUser(token));
    }

    public EkasaResponseDto getReceiptQrCodeDetails(BillDto bill)  {
        return expenseService.getReceiptQrCodeDetails(bill);
    }
}
