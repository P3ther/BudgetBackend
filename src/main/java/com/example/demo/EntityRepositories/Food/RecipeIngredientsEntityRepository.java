package com.example.demo.EntityRepositories.Food;

import com.example.demo.Entity.Food.RecipeIngredientsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecipeIngredientsEntityRepository extends JpaRepository<RecipeIngredientsEntity, Long> {
    List<RecipeIngredientsEntity> findByIdIn(List<Long> ids);
}
