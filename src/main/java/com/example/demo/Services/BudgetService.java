package com.example.demo.Services;

import com.example.demo.Common.CurrencyHelper;
import com.example.demo.Dto.BudgetDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.ExpenseMapper;
import com.example.demo.Mappers.IncomeMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.*;
import java.util.*;

@Service
public class BudgetService {
    private final ExpenseRepository expenseRepository;
    private final IncomeRepository incomeRepository;
    private final CategoryRepository categoryRepository;
    private final ExpenseMapper expenseMapper;
    private final IncomeMapper incomeMapper;
    private final UserEntityRepository userRepository;
    private final CurrencyPairsEntityRepository currencyPairsEntityRepository;
    private final CurrencyHelper currencyHelper;
    private final InvestmentDataService investmentDataService;

    public BudgetService(ExpenseRepository expenseRepository, IncomeRepository incomeRepository, CategoryRepository categoryRepository, ExpenseMapper expenseMapper, IncomeMapper incomeMapper, UserEntityRepository userRepository, CurrencyPairsEntityRepository currencyPairsEntityRepository, CurrencyHelper currencyHelper, InvestmentDataService investmentDataService) {
        this.expenseRepository = expenseRepository;
        this.incomeRepository = incomeRepository;
        this.categoryRepository = categoryRepository;
        this.expenseMapper = expenseMapper;
        this.incomeMapper = incomeMapper;
        this.userRepository = userRepository;
        this.currencyPairsEntityRepository = currencyPairsEntityRepository;
        this.currencyHelper = currencyHelper;
        this.investmentDataService = investmentDataService;
    }

    public BudgetDto getMonthlyBudget(Integer year, Integer month, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        LocalDateTime startOfMonth = LocalDateTime.of(year, month, 1, 0, 0, 0);
        int lastDay = LocalDate.of(year, month, 1).getMonth().length(LocalDate.of(year, month, 1).isLeapYear());
        LocalDateTime endOfMonth = LocalDateTime.of(year, month, lastDay, 23, 59,59);
        List<ExpenseEntity> expenseEntities = expenseRepository.findAllByDateBetweenAndInvestmentExpenseIsNullAndUserWithAccessExpense_UsernameOrderByDateDesc(
                startOfMonth,
                endOfMonth,
                user
        );
        List<IncomeEntity> incomeEntities = incomeRepository.findAllByDateBetweenAndUserWithAccessIncome_UsernameOrderByDateDesc(startOfMonth, endOfMonth, user);
        List<CategoryEntity> categoryEntityListIncome = categoryRepository.findAllByTypeAndUserWithAccessCategory_Username(1, user);
        List<CategoryEntity> categoryEntityListExpense = categoryRepository.findAllByTypeAndUserWithAccessCategory_Username(0, user);
        BudgetDto budgetDto = new BudgetDto(
                1L,
                expenseMapper.convertToListDto(expenseEntities),
                incomeMapper.convertToListDto(incomeEntities),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                investmentDataService.getTotalInvestmentValue(user),
                investmentDataService.getTotalInvestmentGain(user)
        );
        for (ExpenseEntity expenseEntity: expenseEntities) {
            if (!expenseEntity.getExpenseCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(expenseEntity.getExpenseCurrency().getId(), currencyPairsEntityList, userEntity);
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().subtract(expenseEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN)));
                budgetDto.setTotalExpense(budgetDto.getTotalExpense().add(expenseEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2,  RoundingMode.HALF_EVEN)));
            } else {
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().subtract(expenseEntity.getPrice()));
                budgetDto.setTotalExpense(budgetDto.getTotalExpense().add(expenseEntity.getPrice()));
            }
        }
        for (IncomeEntity incomeEntity: incomeEntities) {
            if(!incomeEntity.getIncomeCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(incomeEntity.getIncomeCurrency().getId(), currencyPairsEntityList, userEntity);
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().add(incomeEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN)));
                budgetDto.setTotalIncome(budgetDto.getTotalIncome().add(incomeEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN)));
            } else {
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().add(incomeEntity.getPrice()));
                budgetDto.setTotalIncome(budgetDto.getTotalIncome().add(incomeEntity.getPrice()));
            }
        }
        for (CategoryEntity categoryEntity:categoryEntityListIncome) {
            budgetDto.setExpectedIncome(budgetDto.getExpectedIncome().add(categoryEntity.getLimitingValue()));
        }
        for (CategoryEntity categoryEntity:categoryEntityListExpense) {
            budgetDto.setExpectedExpense(budgetDto.getExpectedExpense().add(categoryEntity.getLimitingValue()));
        }
        return budgetDto;
    }
    public BudgetDto getTimedBudget(StartEndTimeDto startEndTimeDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        List<ExpenseEntity> expenseEntities = expenseRepository.findAllByDateBetweenAndInvestmentExpenseIsNullAndUserWithAccessExpense_UsernameOrderByDateDesc(
                startEndTimeDto.getStartDateTime(),
                startEndTimeDto.getEndDateTime(),
                user
        );
        List<IncomeEntity> incomeEntities = incomeRepository.findAllByDateBetweenAndUserWithAccessIncome_UsernameOrderByDateDesc(
                startEndTimeDto.getStartDateTime(),
                startEndTimeDto.getEndDateTime(),
                user
        );
        List<CategoryEntity> categoryEntityListIncome = categoryRepository.findAllByTypeAndUserWithAccessCategory_Username(1, user);
        List<CategoryEntity> categoryEntityListExpense = categoryRepository.findAllByTypeAndUserWithAccessCategory_Username(0, user);
        BudgetDto budgetDto = new BudgetDto(
                1L,
                expenseMapper.convertToListDto(expenseEntities),
                incomeMapper.convertToListDto(incomeEntities),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                BigDecimal.valueOf(0L),
                investmentDataService.getTotalInvestmentValue(user),
                investmentDataService.getTotalInvestmentGain(user)
        );
        for (ExpenseEntity expenseEntity:expenseEntities) {
            if (!expenseEntity.getExpenseCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(expenseEntity.getExpenseCurrency().getId(), currencyPairsEntityList, userEntity);
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().subtract(expenseEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN)));
                budgetDto.setTotalExpense(budgetDto.getTotalExpense().add(expenseEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2,  RoundingMode.HALF_EVEN)));
            } else {
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().subtract(expenseEntity.getPrice()));
                budgetDto.setTotalExpense(budgetDto.getTotalExpense().add(expenseEntity.getPrice()));
            }
        }
        for (IncomeEntity incomeEntity:incomeEntities) {
            if(!incomeEntity.getIncomeCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(incomeEntity.getIncomeCurrency().getId(), currencyPairsEntityList, userEntity);
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().add(incomeEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN)));
                budgetDto.setTotalIncome(budgetDto.getTotalIncome().add(incomeEntity.getPrice().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN)));
            } else {
                budgetDto.setTotalBalance(budgetDto.getTotalBalance().add(incomeEntity.getPrice()));
                budgetDto.setTotalIncome(budgetDto.getTotalIncome().add(incomeEntity.getPrice()));
            }
        }
        for (CategoryEntity categoryEntity:categoryEntityListIncome) {
            if(!categoryEntity.getCategoryCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(categoryEntity.getCategoryCurrency().getId(), currencyPairsEntityList, userEntity);
                budgetDto.setExpectedIncome(
                        budgetDto.getExpectedIncome().add(
                                categoryEntity.getLimitingValue().multiply(
                                        BigDecimal.valueOf(exchangeRate)
                                ).setScale(2, RoundingMode.HALF_EVEN)
                        )
                );
            } else {
                budgetDto.setExpectedIncome(budgetDto.getExpectedIncome().add(categoryEntity.getLimitingValue()));
            }

        }
        for (CategoryEntity categoryEntity:categoryEntityListExpense) {
            if(!categoryEntity.getCategoryCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(categoryEntity.getCategoryCurrency().getId(), currencyPairsEntityList, userEntity);
                budgetDto.setExpectedExpense(
                        budgetDto.getExpectedExpense().add(
                                categoryEntity.getLimitingValue().multiply(
                                        BigDecimal.valueOf(exchangeRate)
                                ).setScale(2, RoundingMode.HALF_EVEN)
                        )
                );
            } else {
                budgetDto.setExpectedExpense(budgetDto.getExpectedExpense().add(categoryEntity.getLimitingValue()));
            }

        }
        return budgetDto;
    }
}
