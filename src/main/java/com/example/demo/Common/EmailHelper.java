package com.example.demo.Common;

import org.springframework.stereotype.Component;

@Component
public class EmailHelper {
    public String generateEmailBody(String username, String resetCode) {
        String style = ".budgetButton {"
                    + "appearance: none;"
                    + "background-color: #6ea672;"
                    + "border: 1px solid rgba(27, 31, 35, .15);"
                    + "border-radius: 6px;"
                    + "box-shadow: rgba(27, 31, 35, .1) 0 1px 0;"
                    + "box-sizing: border-box;"
                    + "color: #fff;"
                    + "cursor: pointer;"
                    + "display: inline-block;"
                    + "font-family: -apple-system,system-ui,\"Segoe UI\",Helvetica,Arial,sans-serif,\"Apple Color Emoji\",\"Segoe UI Emoji\";"
                    + "font-size: 14px;"
                    + "font-weight: 600;"
                    + "line-height: 20px;"
                    + "padding: 6px 16px;"
                    + "position: relative;"
                    + "text-align: center;"
                    + "text-decoration: none;"
                    + "user-select: none;"
                    + "-webkit-user-select: none;"
                    + "touch-action: manipulation;"
                    + "vertical-align: middle;"
                    + "white-space: nowrap;"
                    + "}"
                    + ".budgetButton:focus:not(:focus-visible):not(.focus-visible) {"
                    + "box-shadow: none;"
                    + "outline: none;"
                    + "}"
                    + ".budgetButton:hover {"
                    + "background-color:#7fb182;"
                    + "}"
                    + ".budgetButton:focus {"
                    + "box-shadow: rgba(46, 164, 79, .4) 0 0 0 3px;"
                    + "outline: none;"
                    + "}"
                    + ".budgetButton:active {"
                    + "background-color: #298e46;"
                    + "box-shadow: rgba(20, 70, 32, .2) 0 1px 0 inset;"
                    + "}";
        String buttonStyle = "background-color: #6ea672;";
        String headerStyle = "color: #4d908e";
        String divStyle = "width: 100%; text-align: center;";
        return "<style>" + style + "</style><div style=\""+ divStyle +"\"><h2 style=\""+ headerStyle +"\">Dear "+ username + "</h2>,<br>" + ""
                + "New code for password reset was generated, and it is: <br>"
                + "<h3>" + resetCode.substring(0,4) + "-" + resetCode.substring(4,8)+ "</h3><br>"
                + "Click this button to regenerate password<br>"
                + "<a href=\"https://ecstatic-cori-83fe24.netlify.app/resetPassword/\" target=\"_blank\">\n"
                + "    <input type=\"submit\" value=\"Regenerate Password\" class=\"budgetButton\" />\n"
                + "</a>\n</div>";
    }
}
