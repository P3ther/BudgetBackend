package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvestmentDto {
    private Long id;
    private LocalDateTime dateTime;
    private BigDecimal investment;
    private BigDecimal gain;
    private Long investmentPlatformId;
    private String investmentPlatformName;
    private Long currencyId;
    private String currencySign;
    private String user;
}
