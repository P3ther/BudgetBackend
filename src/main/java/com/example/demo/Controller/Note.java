package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.NoteInterface;
import com.example.demo.Dto.NoteDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.NoteMapper;
import com.example.demo.Services.NoteService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Note implements NoteInterface {
    private final NoteService noteService;
    private final NoteMapper noteMapper;
    private final UserHelper userHelper;

    public Note(NoteService noteService, NoteMapper noteMapper, UserHelper userHelper) {
        this.noteService = noteService;
        this.noteMapper = noteMapper;
        this.userHelper = userHelper;
    }

    public NoteDto getNote(Long id, String token) throws EntityNotFoundException {
        return noteMapper.convertToDto(noteService.getNote(id, userHelper.getUser(token)));
    }

    public NoteDto createNote(NoteDto noteDto, String token) throws EntityNotFoundException {
        return noteMapper.convertToDto(
                noteService.createNote(
                        noteMapper.convertToEntity(noteDto, userHelper.getUser(token))
                )
        );
    }

    public NoteDto updateNote(NoteDto noteDto, String token) throws EntityNotFoundException {
        return noteMapper.convertToDto(
                noteService.updateEntity(
                        noteMapper.convertToEntity(noteDto, userHelper.getUser(token))
                )
        );
    }

    public List<NoteDto> listNotes(String token) {
        return noteMapper.convertToListDto(noteService.listNotes(userHelper.getUser(token)));
    }

    public void deleteNote(Long id, String token) throws EntityNotFoundException {
        noteService.deleteNote(id, userHelper.getUser(token));
    }
}
