package com.example.demo.Dto.Food;

import com.example.demo.Common.IngredientUnitEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IngredientDto {
    private Long id;
    private String ingredientName;
    private BigDecimal ingredientCalories;
    private IngredientUnitEnum ingredientUnit;
    private List<RecipeIngredientsDtoIngredient> recipeIngredients;
}