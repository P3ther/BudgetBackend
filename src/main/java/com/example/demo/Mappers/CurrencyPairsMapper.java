package com.example.demo.Mappers;

import com.example.demo.Dto.CurrencyPairsDto;
import com.example.demo.Entity.CurrencyEntity;
import com.example.demo.Entity.CurrencyPairsEntity;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CurrencyPairsMapper {
    private final CurrencyEntityRepository currencyEntityRepository;

    public CurrencyPairsMapper(CurrencyEntityRepository currencyEntityRepository) {
        this.currencyEntityRepository = currencyEntityRepository;
    }

    public CurrencyPairsDto convertToDto(CurrencyPairsEntity currencyPairsEntity) {
        return CurrencyPairsDto
                .builder()
                .id(currencyPairsEntity.getId())
                .firstCurrencyId(currencyPairsEntity.getFirstCurrency().getId())
                .firstCurrencyCode(currencyPairsEntity.getFirstCurrency().getCurrencyCode())
                .firstCurrencyName(currencyPairsEntity.getFirstCurrency().getName())
                .firstCurrencySign(currencyPairsEntity.getFirstCurrency().getSign())
                .secondCurrencyId(currencyPairsEntity.getSecondCurrency().getId())
                .secondCurrencyCode(currencyPairsEntity.getSecondCurrency().getCurrencyCode())
                .secondCurrencyName(currencyPairsEntity.getSecondCurrency().getName())
                .secondCurrencySign(currencyPairsEntity.getSecondCurrency().getSign())
                .exchangeRate(currencyPairsEntity.getExchangeRate())
                .lastChange(currencyPairsEntity.getLastChange())
                .build();
    }

    public CurrencyPairsEntity convertToEntity(CurrencyPairsDto currencyPairsDto) {
        CurrencyEntity firstCurrency = currencyEntityRepository.findById(currencyPairsDto.getFirstCurrencyId()).orElse(null);
        CurrencyEntity secondCurrency =  currencyEntityRepository.findById(currencyPairsDto.getSecondCurrencyId()).orElse(null);
        return CurrencyPairsEntity
                .builder()
                .id(currencyPairsDto.getId() != null ? currencyPairsDto.getId()  : -1)
                .firstCurrency(firstCurrency)
                .secondCurrency(secondCurrency)
                .exchangeRate(currencyPairsDto.getExchangeRate())
                .lastChange(currencyPairsDto.getLastChange())
                .build();
    }

    public List<CurrencyPairsDto> convertToListDto(List<CurrencyPairsEntity> currencyPairsEntityList) {
        List<CurrencyPairsDto> currencyPairsDtoList = new ArrayList<>();
        for(CurrencyPairsEntity currencyPairsEntity : currencyPairsEntityList) {
            currencyPairsDtoList.add(convertToDto(currencyPairsEntity));
        }
        return currencyPairsDtoList;
    }
    public List<CurrencyPairsEntity> convertToListEntity(List<CurrencyPairsDto> currencyPairsDtoList) {
        List<CurrencyPairsEntity> currencyPairsEntityList = new ArrayList<>();
        for(CurrencyPairsDto currencyPairsDto : currencyPairsDtoList) {
            currencyPairsEntityList.add(convertToEntity(currencyPairsDto));
        }
        return currencyPairsEntityList;
    }
}
