package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SavingsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal value;
    private LocalDateTime date;
    @ManyToOne()
    @JoinColumn(name = "savingsCurrency")
    private CurrencyEntity savingsCurrency;
    @ManyToOne()
    @JoinColumn(name = "userSaving")
    private UserEntity userSaving;
}
