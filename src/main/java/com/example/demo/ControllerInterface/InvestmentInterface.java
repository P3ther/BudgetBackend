package com.example.demo.ControllerInterface;

import com.example.demo.Dto.InvestmentDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

public interface InvestmentInterface {
    @PostMapping("/investment/")
    InvestmentDto addInvestment(@RequestBody InvestmentDto investmentDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;
}
