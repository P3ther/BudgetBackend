package com.example.demo.Dto;

import com.example.demo.Common.UserRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
    private Long id;
    @NotBlank
    private String username;
    @Pattern(regexp="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$",message="Password doesn't mach required criteria")
    private String password;
    private Long registrationCode;
    private UserRoleEnum userRole;
    @Email()
    private String userEmail;
    private String forgotPasswordCode;
    private CurrencyDto defaultCurrency;
    private ThemeDto defaultTheme;
}
