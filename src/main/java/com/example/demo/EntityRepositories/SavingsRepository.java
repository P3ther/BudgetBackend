package com.example.demo.EntityRepositories;

import com.example.demo.Entity.SavingsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SavingsRepository extends JpaRepository<SavingsEntity, Long>  {
    List<SavingsEntity> findAllByUserSaving_Username(String username);
    Optional<SavingsEntity> findByDateBetweenAndUserSaving_Username(LocalDateTime startDate, LocalDateTime endDate, String username);
    List<SavingsEntity> findAllByDateBetweenAndUserSaving_Username(LocalDateTime startDate, LocalDateTime endDate, String username);
}
