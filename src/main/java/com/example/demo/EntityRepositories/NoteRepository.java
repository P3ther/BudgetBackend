package com.example.demo.EntityRepositories;

import com.example.demo.Entity.NoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface NoteRepository extends JpaRepository<NoteEntity, Long> {
    Optional<NoteEntity> findByIdAndUserEntity_Username(Long id, String username);
    List<NoteEntity> findAllByUserEntity_Username(String username);
}
