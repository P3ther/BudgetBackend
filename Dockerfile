FROM openjdk:15-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring

ENV POSTGRES_HOST=FILL_YOUR_VALUE
ENV POSTGRES_PORT=FILL_YOUR_VALUE
ENV POSTGRES_DB=FILL_YOUR_VALUE
ENV POSTGRES_USER=FILL_YOUR_VALUE
ENV POSTGRES_PASSWORD=FILL_YOUR_VALUE

ENV EMAIL_HOST=FILL_YOUR_VALUE
ENV EMAIL_PORT=FILL_YOUR_VALUE
ENV EMAIL_USERNAME=FILL_YOUR_VALUE
ENV EMAIL_PASSWORD=FILL_YOUR_VALUE

USER spring:spring

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]