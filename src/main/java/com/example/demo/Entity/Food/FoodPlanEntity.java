package com.example.demo.Entity.Food;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FoodPlanEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    @ManyToOne()
    @JoinColumn(name = "dayBreakfast")
    private RecipeEntity breakfast;
    @ManyToOne()
    @JoinColumn(name = "dayLunch")
    private RecipeEntity lunch;
    @ManyToOne()
    @JoinColumn(name = "dayDinner")
    private RecipeEntity dinner;
}