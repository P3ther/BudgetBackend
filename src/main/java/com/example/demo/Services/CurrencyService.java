package com.example.demo.Services;

import com.example.demo.Entity.CurrencyEntity;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public class CurrencyService {
    private final CurrencyEntityRepository currencyEntityRepository;
    private final CurrencyPairsService currencyPairsService;


    public CurrencyService(CurrencyEntityRepository currencyEntityRepository, CurrencyPairsService currencyPairsService) {
        this.currencyEntityRepository = currencyEntityRepository;
        this.currencyPairsService = currencyPairsService;
    }

    public List<CurrencyEntity> getCurrencies() {
        return currencyEntityRepository.findAll();
    }

    public List<CurrencyEntity> addNewCurrency(CurrencyEntity currencyEntity) {
        currencyEntityRepository.save(currencyEntity);
        currencyPairsService.addNewCurrency(currencyEntity);
        return currencyEntityRepository.findAll();
    }

    public List<CurrencyEntity> editCurrency(CurrencyEntity currencyEntity) throws ParseException {
        CurrencyEntity returnedEntity = currencyEntityRepository.findById(currencyEntity.getId()).orElse(null);
        returnedEntity.setName(currencyEntity.getName());
        returnedEntity.setCurrencyCode(currencyEntity.getCurrencyCode());
        returnedEntity.setSign(currencyEntity.getSign());
        currencyEntityRepository.save(returnedEntity);
        currencyPairsService.updateExchangeRates();
        return currencyEntityRepository.findAll();
    }
}
