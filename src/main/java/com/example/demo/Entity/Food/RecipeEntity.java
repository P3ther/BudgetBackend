package com.example.demo.Entity.Food;

import com.example.demo.Entity.UserEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecipeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String recipeName;
    @Column(columnDefinition = "TEXT")
    private String recipeInstructions;
    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RecipeIngredientsEntity> recipeIngredientEntities;
    private LocalDateTime lastDateTimeUsed;
    @ManyToOne()
    @JoinColumn(name = "userRecipe")
    private UserEntity userEntity;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "breakfast")
    private List<FoodPlanEntity> foodPlanBreakfastList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lunch")
    private List<FoodPlanEntity> foodPlanLunchList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dinner")
    private List<FoodPlanEntity> foodPlanDinnerList;
}
