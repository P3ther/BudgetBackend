package com.example.demo.EntityRepositories;

import com.example.demo.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserEntityRepository extends JpaRepository<UserEntity, Long> {
    // UserEntity findByUsername(String username);
    Optional<UserEntity> findByUsername(String username);
    Optional<UserEntity> findByUserEmail(String userEmail);
}
