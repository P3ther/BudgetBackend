package com.example.demo.EntityRepositories;

import com.example.demo.Entity.InvestmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface InvestmentEntityRepository extends JpaRepository<InvestmentEntity, Long>  {
    List<InvestmentEntity> findAllByUserWithAccessInvestment_Username(String userName);
    List<InvestmentEntity> findAllByGainNotNullAndUserWithAccessInvestment_Username(String userName);
    List<InvestmentEntity> findAllByDateTimeBetweenAndUserWithAccessInvestment_Username(LocalDateTime startDate, LocalDateTime endTime, String userName);
    List<InvestmentEntity> findAllByDateTimeBeforeAndUserWithAccessInvestment_Username(LocalDateTime endTime, String userName);
    List<InvestmentEntity> findAllByDateTimeBeforeAndInvestmentPlatform_IdAndUserWithAccessInvestment_Username(LocalDateTime endTime, Long investmentPlatformId, String userName);
}
