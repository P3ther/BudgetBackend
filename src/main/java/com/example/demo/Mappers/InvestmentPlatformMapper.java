package com.example.demo.Mappers;

import com.example.demo.Common.CurrencyHelper;
import com.example.demo.Dto.InvestmentPlatformDto;
import com.example.demo.Entity.CurrencyPairsEntity;
import com.example.demo.Entity.InvestmentEntity;
import com.example.demo.Entity.InvestmentPlatformEntity;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Component
public class InvestmentPlatformMapper {
    private final InvestmentMapper investmentMapper;
    private final UserEntityRepository userRepository;
    private final CurrencyHelper currencyHelper;
    private final CurrencyPairsEntityRepository currencyPairsRepository;

    public InvestmentPlatformMapper(InvestmentMapper investmentMapper, UserEntityRepository userRepository, CurrencyHelper currencyHelper, CurrencyPairsEntityRepository currencyPairsRepository) {
        this.investmentMapper = investmentMapper;
        this.userRepository = userRepository;
        this.currencyHelper = currencyHelper;
        this.currencyPairsRepository = currencyPairsRepository;
    }

    public InvestmentPlatformDto convertToDto(InvestmentPlatformEntity investmentPlatformEntity) {
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsRepository.findAll();
        BigDecimal totalValue = BigDecimal.valueOf(0L);
        BigDecimal totalGain = BigDecimal.valueOf(0L);
        for(InvestmentEntity investmentEntity : investmentPlatformEntity.getInvestmentsOnPlatform()) {
            if (investmentEntity.getInvestment() != null) {
                if(!investmentEntity.getInvestmentCurrency().getId().equals(investmentPlatformEntity.getUserWithAccessInvestmentPlatform().getDefaultCurrency().getId())) {
                    double exchangeRate = currencyHelper.getExchangeRate(investmentEntity.getInvestmentCurrency().getId(), currencyPairsEntityList, investmentPlatformEntity.getUserWithAccessInvestmentPlatform());
                    totalValue = totalValue.add(investmentEntity.getInvestment().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN));
                } else {
                    totalValue = totalValue.add(investmentEntity.getInvestment());
                }
            } else if (investmentEntity.getGain() != null) {
                if(!investmentEntity.getInvestmentCurrency().getId().equals(investmentPlatformEntity.getUserWithAccessInvestmentPlatform().getDefaultCurrency().getId())) {
                    double exchangeRate = currencyHelper.getExchangeRate(investmentEntity.getInvestmentCurrency().getId(), currencyPairsEntityList, investmentPlatformEntity.getUserWithAccessInvestmentPlatform());
                    totalValue = totalValue.add(investmentEntity.getGain().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN));
                    totalGain = totalGain.add(investmentEntity.getGain().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2, RoundingMode.HALF_EVEN));
                } else {
                    totalValue = totalValue.add(investmentEntity.getGain());
                    totalGain = totalGain.add((investmentEntity.getGain()));
                }
            }
        }

        return InvestmentPlatformDto.builder()
                .id(investmentPlatformEntity.getId())
                .name(investmentPlatformEntity.getName())
                .investmentsOnPlatform(investmentMapper.convertToListDto(investmentPlatformEntity.getInvestmentsOnPlatform()))
                .totalValue(totalValue)
                .totalGain(totalGain)
                .build();
    }
    public InvestmentPlatformEntity convertToEntity(InvestmentPlatformDto investmentPlatformDto, String user) throws EntityNotFoundException {
        return InvestmentPlatformEntity.builder()
                .id(investmentPlatformDto.getId())
                .name(investmentPlatformDto.getName())
                .investmentsOnPlatform(investmentMapper.convertToListEntity(investmentPlatformDto.getInvestmentsOnPlatform(), user))
                .userWithAccessInvestmentPlatform(userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new))
                .build();
    }
    public List<InvestmentPlatformDto> convertToListDto(List<InvestmentPlatformEntity> investmentPlatformEntities) {
        List<InvestmentPlatformDto> investmentPlatformDtoList = new ArrayList<>();
        for (InvestmentPlatformEntity investmentPlatformEntity : investmentPlatformEntities) {
            investmentPlatformDtoList.add(convertToDto(investmentPlatformEntity));
        }
        return investmentPlatformDtoList;
    }
}
