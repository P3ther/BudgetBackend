package com.example.demo.Mappers;

import com.example.demo.Common.CategoryHelper;
import com.example.demo.Dto.CategoryDto;
import com.example.demo.Entity.CategoryEntity;
import com.example.demo.Entity.CurrencyEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryMapper {

    private final UserEntityRepository userRepository;
    private final CurrencyEntityRepository currencyEntityRepository;
    private final CategoryHelper categoryHelper;
    private final ExpenseMapper expenseMapper;
    private final IncomeMapper incomeMapper;

    public CategoryMapper(UserEntityRepository userRepository, CurrencyEntityRepository currencyEntityRepository, CategoryHelper categoryHelper, ExpenseMapper expenseMapper, IncomeMapper incomeMapper) {
        this.userRepository = userRepository;
        this.currencyEntityRepository = currencyEntityRepository;
        this.categoryHelper = categoryHelper;
        this.expenseMapper = expenseMapper;
        this.incomeMapper = incomeMapper;
    }

    public CategoryDto convertToDto(CategoryEntity categoryEntity, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        BigDecimal actualValue = BigDecimal.valueOf(0L);
        if(categoryEntity.getExpenseList() != null || categoryEntity.getIncomeList() != null) {
            actualValue = categoryHelper.getCategoryActualValue(categoryEntity, userEntity);
        }
        return CategoryDto.builder()
                .id(categoryEntity.getId())
                .name(categoryEntity.getName())
                .limit(categoryEntity.getLimitingValue())
                .value(actualValue)
                .type(categoryEntity.getType())
                .expenseList(categoryEntity.getExpenseList() == null? null : expenseMapper.convertToListDto(categoryEntity.getExpenseList()))
                .incomeList(categoryEntity.getIncomeList() == null? null : incomeMapper.convertToListDto(categoryEntity.getIncomeList()))
                .currencyId(categoryEntity.getCategoryCurrency() == null ? -1 : categoryEntity.getCategoryCurrency().getId())
                .currencySign(categoryEntity.getCategoryCurrency() == null ? "" : categoryEntity.getCategoryCurrency().getSign())
                .build();
    }

    public CategoryEntity convertToEntity(CategoryDto categoryDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        CurrencyEntity currencyEntity;
        if(categoryDto.getCurrencyId() != null) {
            currencyEntity = currencyEntityRepository.findById(categoryDto.getCurrencyId()).orElse(null);
        } else {
            currencyEntity = null;
        }
        return CategoryEntity.builder()
                .id(categoryDto.getId())
                .name(categoryDto.getName())
                .limitingValue(categoryDto.getLimit())
                .type(categoryDto.getType())
                .userWithAccessCategory(userEntity)
                .categoryCurrency(currencyEntity)
                .build();
    }

    public List<CategoryDto> convertToListDto(List<CategoryEntity> categoryEntityList, String user) throws EntityNotFoundException {
        List<CategoryDto> categoryDtoList = new ArrayList<>();
        for (CategoryEntity categoryEntity : categoryEntityList) {
            categoryDtoList.add(convertToDto(categoryEntity, user));
        }
        return categoryDtoList;
    }
}
