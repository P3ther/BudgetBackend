package com.example.demo.Controller;

import com.example.demo.ControllerInterface.CurrencyInterface;
import com.example.demo.Dto.CurrencyDto;
import com.example.demo.Mappers.CurrencyMapper;
import com.example.demo.Services.CurrencyService;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;

@RestController
public class Currency implements CurrencyInterface {

    private final CurrencyService currencyService;
    private final CurrencyMapper currencyMapper;

    public Currency(CurrencyService currencyService, CurrencyMapper currencyMapper) {
        this.currencyService = currencyService;
        this.currencyMapper = currencyMapper;
    }

    public List<CurrencyDto> getCurrencies() {
        return currencyMapper.convertToListDto(currencyService.getCurrencies());
    }

    public List<CurrencyDto> addNewCurrency(CurrencyDto currencyDto) {
        return currencyMapper.convertToListDto(
                currencyService.addNewCurrency(
                        currencyMapper.convertToEntity(currencyDto)
                )
        );
    }

    public List<CurrencyDto> editCurrency(CurrencyDto currencyDto) throws ParseException {
        return currencyMapper.convertToListDto(
                currencyService.editCurrency(
                        currencyMapper.convertToEntity(currencyDto)
                )
        );
    }
}
