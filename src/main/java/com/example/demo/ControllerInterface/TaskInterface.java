package com.example.demo.ControllerInterface;

import com.example.demo.Dto.TaskDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface TaskInterface {

    @GetMapping("/task/{id}/")
    TaskDto getTask(@PathVariable("id") Long id,  @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/task/")
    TaskDto createTask(@RequestBody TaskDto taskDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PutMapping("/task/")
    TaskDto updateTask(@RequestBody TaskDto taskDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/tasks/")
    List<TaskDto> listTasks(@RequestHeader("Authorization") String token);

    @DeleteMapping("/task/{id}/")
    void removeTask(@PathVariable("id") Long id,  @RequestHeader("Authorization") String token) throws EntityNotFoundException;
}
