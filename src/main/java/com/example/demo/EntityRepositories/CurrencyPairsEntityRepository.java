package com.example.demo.EntityRepositories;

import com.example.demo.Entity.CurrencyPairsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyPairsEntityRepository extends JpaRepository<CurrencyPairsEntity, Long> {
}
