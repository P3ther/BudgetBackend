package com.example.demo.ControllerInterface;

import com.example.demo.Dto.InvestmentPlatformDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

public interface InvestmentPlatformInterface {

    @GetMapping("/investmentPlatform/{id}/")
    InvestmentPlatformDto getInvestmentPlatform(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/investmentPlatform/")
    InvestmentPlatformDto createInvestmentPlatform(@Valid @RequestBody InvestmentPlatformDto investmentPlatformDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PutMapping("/investmentPlatform/")
    InvestmentPlatformDto updateInvestmentPlatform(@Valid @RequestBody InvestmentPlatformDto investmentPlatformDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @DeleteMapping("/investmentPlatform/{id}/")
    void deleteInvestmentPlatform(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/investmentPlatforms/")
    List<InvestmentPlatformDto> listInvestmentPlatforms(@RequestHeader("Authorization") String token);

}
