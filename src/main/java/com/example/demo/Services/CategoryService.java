package com.example.demo.Services;

import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Entity.*;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final IncomeRepository  incomeRepository;
    private final ExpenseRepository expenseRepository;

    public CategoryService(CategoryRepository categoryRepository, IncomeRepository incomeRepository, ExpenseRepository expenseRepository) {
        this.categoryRepository = categoryRepository;
        this.incomeRepository = incomeRepository;
        this.expenseRepository = expenseRepository;
    }

    public CategoryEntity readCategory(Long id, String username) throws EntityNotFoundException {
        return categoryRepository.findByIdAndUserWithAccessCategory_Username(id, username).orElseThrow(EntityNotFoundException::new);
    }
    public CategoryEntity readCategoryByTime(Long id, StartEndTimeDto startEndTimeDto, String username) throws EntityNotFoundException {
        CategoryEntity categoryEntity = categoryRepository.findByIdAndUserWithAccessCategory_Username(id, username).orElseThrow(EntityNotFoundException::new);
        categoryEntity.getIncomeList().removeIf(incomeEntity -> incomeEntity.getDate().isBefore(startEndTimeDto.getStartDateTime()) || incomeEntity.getDate().isAfter(startEndTimeDto.getEndDateTime()));
        categoryEntity.getExpenseList().removeIf(expenseEntity -> expenseEntity.getDate().isBefore(startEndTimeDto.getStartDateTime()) || expenseEntity.getDate().isAfter(startEndTimeDto.getEndDateTime()));
        return categoryEntity;
    }

    public List<CategoryEntity> getCategories(String user) {
        return categoryRepository.findByUserWithAccessCategory_Username(user);
    }
    public List<CategoryEntity> getCategoriesAndValuesForTime(StartEndTimeDto startEndTimeDto, String user) {
        long months = ChronoUnit.MONTHS.between( startEndTimeDto.getStartDateTime(), startEndTimeDto.getEndDateTime());
        List<CategoryEntity> categoryEntityList = categoryRepository.findByUserWithAccessCategory_Username(user);
        for(CategoryEntity categoryEntity : categoryEntityList) {
            if(months > 0) {
                BigDecimal tempLimit = categoryEntity.getLimitingValue().multiply(BigDecimal.valueOf(months));
                categoryEntity.setLimitingValue(tempLimit);
            }
            if(categoryEntity.getExpenseList().size() > 0) {
                List<ExpenseEntity> tempExpenses = categoryEntity.getExpenseList()
                        .stream()
                        .filter(c -> c.getDate().isAfter(
                                startEndTimeDto.getStartDateTime() )&&
                                c.getDate().isBefore(startEndTimeDto.getEndDateTime())).collect(Collectors.toList());
                categoryEntity.setExpenseList(tempExpenses);
            } else if(categoryEntity.getIncomeList().size() > 0) {
                List<IncomeEntity> tempIncomes = categoryEntity.getIncomeList()
                        .stream()
                        .filter(c -> c.getDate().isAfter(
                                startEndTimeDto.getStartDateTime() )&&
                                c.getDate().isBefore(startEndTimeDto.getEndDateTime())).collect(Collectors.toList());
                categoryEntity.setIncomeList(tempIncomes);
            }
        }
        return categoryEntityList;
    }

    public List<CategoryEntity> getExpenseCategories(String user) {
        return categoryRepository.findAllByTypeAndUserWithAccessCategory_Username(0, user);
    }
    public List<CategoryEntity> getIncomeCategories(String user) {
        return categoryRepository.findAllByTypeAndUserWithAccessCategory_Username(1, user);
    }
    public CategoryEntity addCategory(CategoryEntity categoryEntity) {
        return categoryRepository.save(categoryEntity);
    }

    public CategoryEntity updateCategory(CategoryEntity categoryEntity) {
        CategoryEntity categoryEntityEdited = categoryRepository.findById(categoryEntity.getId()).orElse(null);
        categoryEntityEdited.setName(categoryEntity.getName());
        categoryEntityEdited.setLimitingValue(categoryEntity.getLimitingValue());
        categoryEntityEdited.setType(categoryEntity.getType());
        return categoryRepository.save(categoryEntityEdited);
    }

    public void deleteCategory(Long id, String user) throws EntityNotFoundException {
        CategoryEntity categoryEntityFound = categoryRepository.findByIdAndUserWithAccessCategory_Username(id, user).orElseThrow(EntityNotFoundException::new);
        if (categoryEntityFound.getType() == 0) {
            for (ExpenseEntity entity: categoryEntityFound.getExpenseList()) {
                entity.setCategoryExpense(null);
                expenseRepository.save(entity);
            }
        } else {
            for (IncomeEntity entity: categoryEntityFound.getIncomeList()) {
                entity.setCategoryIncome(null);
                incomeRepository.save(entity);
            }
        }
        categoryRepository.delete(categoryEntityFound);
    }
    public void deleteCategoryAndAllItems(Long id, String user) throws EntityNotFoundException {
        CategoryEntity categoryEntityFound = categoryRepository.findByIdAndUserWithAccessCategory_Username(id, user).orElseThrow(EntityNotFoundException::new);
        if (categoryEntityFound.getType() == 0) {
            List<ExpenseEntity> entitiesToDelete = new ArrayList<>(categoryEntityFound.getExpenseList());
            expenseRepository.deleteAll(entitiesToDelete);
        } else {
            List<IncomeEntity> entitiesToDelete = new ArrayList<>(categoryEntityFound.getIncomeList());
            incomeRepository.deleteAll(entitiesToDelete);
        }
        categoryRepository.delete(categoryEntityFound);
    }
}
