package com.example.demo.Mappers;

import com.example.demo.Common.MapperHelper;
import com.example.demo.Dto.SavingsDto;
import com.example.demo.Entity.SavingsEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SavingsMapper {
    private final MapperHelper mapperHelper;
    private final UserEntityRepository userRepository;
    private final CurrencyMapper currencyMapper;

    public SavingsMapper(MapperHelper mapperHelper, UserEntityRepository userRepository, CurrencyMapper currencyMapper) {
        this.mapperHelper = mapperHelper;
        this.userRepository = userRepository;
        this.currencyMapper = currencyMapper;
    }
    public SavingsDto convertToDto(SavingsEntity savingsEntity) {
        return SavingsDto.builder()
                .id(savingsEntity.getId())
                .value(savingsEntity.getValue())
                .date(savingsEntity.getDate())
                .currency(currencyMapper.convertToDto(savingsEntity.getSavingsCurrency()))
                .build();
    }

    public List<SavingsDto> convertToListDto(List<SavingsEntity> savingsEntityList) {
    List<SavingsDto> savingsDtoList = new ArrayList<>();
    for(SavingsEntity savingsEntity: savingsEntityList) {
        savingsDtoList.add(convertToDto(savingsEntity));
    }
    return savingsDtoList;
    }

    private SavingsEntity conventerToEntity(SavingsDto savingsDto, UserEntity userEntity) {
        return SavingsEntity.builder()
                .id(mapperHelper.handleLongNull(savingsDto.getId()))
                .value(savingsDto.getValue())
                .date(savingsDto.getDate())
                .savingsCurrency(currencyMapper.convertToEntity(savingsDto.getCurrency()))
                .userSaving(userEntity)
                .build();
    }
    public SavingsEntity convertToEntity(SavingsDto savingsDto, String username) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username).orElseThrow(EntityNotFoundException::new);
        return conventerToEntity(savingsDto, userEntity);
    }

    public List<SavingsEntity> convertToListEntity(List<SavingsDto> savingsDtoList, String username) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username).orElseThrow(EntityNotFoundException::new);
        List<SavingsEntity> savingsEntityList = new ArrayList<>();
        for(SavingsDto savingsDto: savingsDtoList) {
            savingsEntityList.add(conventerToEntity(savingsDto, userEntity));
        }
        return savingsEntityList;
    }
}
