package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDto {
    private Long id;
    private String name;
    private BigDecimal limit;
    private BigDecimal value;
    private Integer type;
    private List<ExpenseDto> expenseList;
    private List<IncomeDto> incomeList;
    private Long currencyId;
    private String currencySign;
}
