package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private BigDecimal limitingValue;
    private Integer type;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "categoryExpense")
    private List<ExpenseEntity> expenseList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "categoryIncome")
    private List<IncomeEntity> incomeList;
    @ManyToOne()
    @JoinColumn(name = "userWithAccessCategory")
    private UserEntity userWithAccessCategory;
    @ManyToOne()
    @JoinColumn(name = "categoryCurrency")
    private CurrencyEntity categoryCurrency;
}
