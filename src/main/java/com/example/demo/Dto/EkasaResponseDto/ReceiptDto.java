package com.example.demo.Dto.EkasaResponseDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceiptDto {
    private double totalPrice;
    private String createDate;
    private LocalDateTime date;
    private OrganizationDto organization;
}
