package com.example.demo.EntityRepositories.Food;

import com.example.demo.Entity.Food.RecipeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RecipeEntityRepository extends JpaRepository<RecipeEntity, Long> {
    Optional<RecipeEntity> findByIdAndUserEntity_Username(Long id, String username);
    List<RecipeEntity> findAllByUserEntity_Username(String username);
}
