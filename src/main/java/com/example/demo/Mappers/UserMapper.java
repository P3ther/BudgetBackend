package com.example.demo.Mappers;

import com.example.demo.Dto.UserDto;
import com.example.demo.Entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final CurrencyMapper currencyMapper;
    private final ThemeMapper themeMapper;

    public UserMapper(CurrencyMapper currencyMapper, ThemeMapper themeMapper) {
        this.currencyMapper = currencyMapper;
        this.themeMapper = themeMapper;
    }

    public UserDto convertToDto(UserEntity userEntity) {
        return UserDto
                .builder()
                .username(userEntity.getUsername())
                .registrationCode(userEntity.getRegistrationCode())
                .userEmail(userEntity.getUserEmail())
                .forgotPasswordCode(userEntity.getForgotPasswordCode())
                .defaultCurrency(userEntity.getDefaultCurrency() == null ? null : currencyMapper.convertToDto((userEntity.getDefaultCurrency())))
                .defaultTheme(userEntity.getDefaultTheme() == null ? null : themeMapper.convertToDto(userEntity.getDefaultTheme()))
                .build();
    }

    public UserEntity convertToEntity(UserDto userDto) {
        return UserEntity
                .builder()
                .id(userDto.getId() == null ? -1 : userDto.getId())
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .registrationCode(userDto.getRegistrationCode())
                .userEmail(userDto.getUserEmail())
                .forgotPasswordCode(userDto.getForgotPasswordCode())
                .defaultCurrency(userDto.getDefaultCurrency() == null ? null : currencyMapper.convertToEntity(userDto.getDefaultCurrency()))
                .defaultTheme(userDto.getDefaultTheme() == null ? null : themeMapper.convertToEntity(userDto.getDefaultTheme()))
                .build();
    }
}
