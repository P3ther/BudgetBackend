package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExpenseDto {
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    @PastOrPresent
    private LocalDateTime date;
    @NotBlank
    private BigDecimal price;
    @NotBlank
    private String addressToReceipt;
    private Long investmentId;
    private Long category;
    private String categoryName;
    private Long currencyId;
    private String currencySign;
}
