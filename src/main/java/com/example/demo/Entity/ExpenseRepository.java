package com.example.demo.Entity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ExpenseRepository extends JpaRepository<ExpenseEntity, Long> {
    Optional<ExpenseEntity> findByIdAndUserWithAccessExpense_Username(Long id, String userName);
    List<ExpenseEntity> findAllByDateBetweenAndInvestmentExpenseIsNullAndUserWithAccessExpense_Username(LocalDateTime startDate, LocalDateTime endTime, String userName);
    List<ExpenseEntity> findAllByDateBetweenAndCategoryExpense_IdAndInvestmentExpenseIsNullAndUserWithAccessExpense_Username(LocalDateTime startDate, LocalDateTime endTime, Long categoryId,  String userName);
    List<ExpenseEntity> findAllByDateBetweenAndInvestmentExpenseIsNullAndUserWithAccessExpense_UsernameOrderByDateDesc(LocalDateTime startDate, LocalDateTime endTime, String userName);
}
