package com.example.demo.Services;

import com.example.demo.Common.CurrencyHelper;
import com.example.demo.Dto.InvestmentDataDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import com.example.demo.EntityRepositories.InvestmentEntityRepository;
import com.example.demo.EntityRepositories.InvestmentPlatformRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class InvestmentDataService {
    private final InvestmentEntityRepository investmentEntityRepository;
    private final UserEntityRepository userRepository;
    private final CurrencyHelper currencyHelper;
    private final CurrencyPairsEntityRepository currencyPairsEntityRepository;

    public InvestmentDataService(InvestmentEntityRepository investmentEntityRepository, UserEntityRepository userRepository, CurrencyHelper currencyHelper, CurrencyPairsEntityRepository currencyPairsEntityRepository) {
        this.investmentEntityRepository = investmentEntityRepository;
        this.userRepository = userRepository;
        this.currencyHelper = currencyHelper;
        this.currencyPairsEntityRepository = currencyPairsEntityRepository;
    }

    public List<InvestmentDataDto> getInvestmentData(List<LocalDateTime> dateTimes, String user) {
        List<InvestmentDataDto> investmentDataDtoList = new ArrayList<>();
        List<InvestmentEntity> investmentEntityList = investmentEntityRepository.findAllByDateTimeBetweenAndUserWithAccessInvestment_Username(
                dateTimes.get(0),
                dateTimes.get(dateTimes.size() - 1 ),
                user);
        for (int i = 0; i < dateTimes.size(); i++) {
            LocalDateTime tempDateTime = dateTimes.get(i);
            List<InvestmentEntity> investmentEntities = investmentEntityList.stream().filter(c -> c.getDateTime().isBefore(tempDateTime)).collect(Collectors.toList());
            BigDecimal value = BigDecimal.valueOf(0L);
            for (InvestmentEntity investmentEntity : investmentEntities) {
                if(investmentEntity.getInvestment() != null) {
                    value = value.add(investmentEntity.getInvestment());
                } else if(investmentEntity.getGain() != null) {
                    value = value.add(investmentEntity.getGain());
                }
            }
            investmentDataDtoList.add(
                    InvestmentDataDto
                            .builder()
                            .value(value)
                            .dateTime(dateTimes.get(i))
                            .build());
        }
        return investmentDataDtoList;
    }

    public BigDecimal getTotalInvestmentValue(String user) throws EntityNotFoundException {
        BigDecimal totalValue = BigDecimal.valueOf(0L);
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        List<InvestmentEntity> investmentEntities = investmentEntityRepository.findAllByUserWithAccessInvestment_Username(user);
        for (InvestmentEntity entity: investmentEntities) {
            if(entity.getInvestment() != null) {
                if (!entity.getInvestmentCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                    double exchangeRate = currencyHelper.getExchangeRate(entity.getInvestmentCurrency().getId(), currencyPairsEntityList, userEntity);
                    totalValue = totalValue.add(
                            entity.getInvestment().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2,  RoundingMode.HALF_EVEN)
                    );
                } else {
                    totalValue = totalValue.add(entity.getInvestment());
                }

            } else if (entity.getGain() != null) {
                if (!entity.getInvestmentCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                    double exchangeRate = currencyHelper.getExchangeRate(entity.getInvestmentCurrency().getId(), currencyPairsEntityList, userEntity);
                    totalValue = totalValue.add(
                            entity.getGain().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2,  RoundingMode.HALF_EVEN)
                    );
                } else {
                    totalValue = totalValue.add(entity.getGain());
                }

            }
        }
        return totalValue;
    }

    public BigDecimal getTotalInvestmentGain(String user) throws EntityNotFoundException {
        BigDecimal totalValue = BigDecimal.valueOf(0L);
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        List<InvestmentEntity> investmentEntities = investmentEntityRepository.findAllByGainNotNullAndUserWithAccessInvestment_Username(user);
        for (InvestmentEntity entity: investmentEntities) {
            if (!entity.getInvestmentCurrency().getId().equals(userEntity.getDefaultCurrency().getId())) {
                double exchangeRate = currencyHelper.getExchangeRate(entity.getInvestmentCurrency().getId(), currencyPairsEntityList, userEntity);
                totalValue = totalValue.add(
                        entity.getGain().multiply(BigDecimal.valueOf(exchangeRate)).setScale(2,  RoundingMode.HALF_EVEN)
                );
            } else {
                totalValue = totalValue.add(entity.getGain());
            }
        }
        return totalValue;
    }

    public List<InvestmentDataDto> getInvestmentValue(List<LocalDateTime> dateTimes, String user) {
        List<InvestmentDataDto> investmentDataDtoList = new ArrayList<>();
        List<InvestmentEntity> investmentEntityList = investmentEntityRepository.findAllByDateTimeBeforeAndUserWithAccessInvestment_Username(
                dateTimes.get(dateTimes.size() - 1 ),
                user);
        for (LocalDateTime tempDateTime : dateTimes) {
            List<InvestmentEntity> investmentEntities = investmentEntityList.stream().filter(c -> c.getDateTime().isBefore(tempDateTime)).collect(Collectors.toList());
            BigDecimal value = BigDecimal.valueOf(0L);
            for (InvestmentEntity investmentEntity : investmentEntities) {
                if (investmentEntity.getInvestment() != null) {
                    value = value.add(investmentEntity.getInvestment());
                }
            }
            investmentDataDtoList.add(
                    InvestmentDataDto
                            .builder()
                            .value(value)
                            .dateTime(tempDateTime)
                            .build());
        }
        return investmentDataDtoList;
    }

    public List<InvestmentDataDto> getInvestmentGain(List<LocalDateTime> dateTimes, String user) {
        List<InvestmentDataDto> investmentDataDtoList = new ArrayList<>();
        List<InvestmentEntity> investmentEntityList = investmentEntityRepository.findAllByDateTimeBeforeAndUserWithAccessInvestment_Username(
                dateTimes.get(dateTimes.size() - 1 ),
                user);
        for (int i = 0; i < dateTimes.size(); i++) {
            LocalDateTime tempDateTime = dateTimes.get(i);
            List<InvestmentEntity> investmentEntities = investmentEntityList.stream().filter(c -> c.getDateTime().isBefore(tempDateTime)).collect(Collectors.toList());
            BigDecimal value = BigDecimal.valueOf(0L);
            for (InvestmentEntity investmentEntity : investmentEntities) {
                if(investmentEntity.getGain() != null) {
                    value = value.add(investmentEntity.getGain());
                }
            }
            investmentDataDtoList.add(
                    InvestmentDataDto
                            .builder()
                            .value(value)
                            .dateTime(dateTimes.get(i))
                            .build());
        }
        return investmentDataDtoList;
    }

    public List<InvestmentDataDto> getInvestmentValueForPlatform(List<LocalDateTime> dateTimes, Long platformId, String user) {
        List<InvestmentDataDto> investmentDataDtoList = new ArrayList<>();
        List<InvestmentEntity> investmentEntityList = investmentEntityRepository.findAllByDateTimeBeforeAndInvestmentPlatform_IdAndUserWithAccessInvestment_Username(
                dateTimes.get(dateTimes.size() - 1 ),
                platformId,
                user);
        for (int i = 0; i < dateTimes.size(); i++) {
            LocalDateTime tempDateTime = dateTimes.get(i);
            List<InvestmentEntity> investmentEntities = investmentEntityList.stream().filter(c -> c.getDateTime().isBefore(tempDateTime)).collect(Collectors.toList());
            BigDecimal value = BigDecimal.valueOf(0L);
            for (InvestmentEntity investmentEntity : investmentEntities) {
                if(investmentEntity.getInvestment() != null) {
                    value = value.add(investmentEntity.getInvestment());
                }
            }
            investmentDataDtoList.add(
                    InvestmentDataDto
                            .builder()
                            .value(value)
                            .dateTime(dateTimes.get(i))
                            .build());
        }
        return investmentDataDtoList;
    }

    public List<InvestmentDataDto> getInvestmentGainForPlatform(List<LocalDateTime> dateTimes, Long platformId, String user) {
        List<InvestmentDataDto> investmentDataDtoList = new ArrayList<>();
        List<InvestmentEntity> investmentEntityList = investmentEntityRepository.findAllByDateTimeBeforeAndInvestmentPlatform_IdAndUserWithAccessInvestment_Username(
                dateTimes.get(dateTimes.size() - 1 ),
                platformId,
                user);
        for (int i = 0; i < dateTimes.size(); i++) {
            LocalDateTime tempDateTime = dateTimes.get(i);
            List<InvestmentEntity> investmentEntities = investmentEntityList.stream().filter(c -> c.getDateTime().isBefore(tempDateTime)).collect(Collectors.toList());
            BigDecimal value = BigDecimal.valueOf(0L);
            for (InvestmentEntity investmentEntity : investmentEntities) {
                if(investmentEntity.getGain() != null) {
                    value = value.add(investmentEntity.getGain());
                }
            }
            investmentDataDtoList.add(
                    InvestmentDataDto
                            .builder()
                            .value(value)
                            .dateTime(dateTimes.get(i))
                            .build());
        }
        return investmentDataDtoList;
    }
}
