package com.example.demo.Services;

import com.example.demo.Entity.NoteEntity;
import com.example.demo.EntityRepositories.NoteRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {
    private final NoteRepository noteRepository;

    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public NoteEntity getNote(Long id, String user) throws EntityNotFoundException {
        return noteRepository.findByIdAndUserEntity_Username(id, user).orElseThrow(EntityNotFoundException::new);
    }

    public NoteEntity createNote(NoteEntity noteEntity) {
        return noteRepository.save(noteEntity);
    }

    public NoteEntity updateEntity(NoteEntity noteEntity) throws EntityNotFoundException {
        NoteEntity editedNote = noteRepository.findByIdAndUserEntity_Username(noteEntity.getId(), noteEntity.getUserEntity().getUsername()).orElseThrow(EntityNotFoundException::new);
        editedNote.setNoteName(noteEntity.getNoteName());
        editedNote.setNoteDetails(noteEntity.getNoteDetails());
        editedNote.setLastUpdated(noteEntity.getLastUpdated());
        return noteRepository.save(editedNote);
    }

    public List<NoteEntity> listNotes(String user) {
        return noteRepository.findAllByUserEntity_Username(user);
    }

    public void deleteNote(Long id, String user)  throws EntityNotFoundException {
        NoteEntity deletedNote = noteRepository.findByIdAndUserEntity_Username(id, user).orElseThrow(EntityNotFoundException::new);
        noteRepository.delete(deletedNote);
    }
}
