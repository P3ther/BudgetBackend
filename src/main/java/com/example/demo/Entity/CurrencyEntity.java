package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrencyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String name;
    @Column(unique = true, nullable = false)
    private String currencyCode;
    @Column(unique = true, nullable = false)
    private String sign;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "defaultCurrency")
    private List<UserEntity> usersListDefaultEntity;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "firstCurrency")
    private List<CurrencyPairsEntity> firstPairedCurrency;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "secondCurrency")
    private List<CurrencyPairsEntity> secondPairedCurrency;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "incomeCurrency")
    private List<IncomeEntity> incomeCurrency;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "expenseCurrency")
    private List<ExpenseEntity> expenseCurrency;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "categoryCurrency")
    private List<CategoryEntity> categoryCurrency;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "investmentCurrency")
    private List<InvestmentEntity> investmentCurrency;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "savingsCurrency")
    private List<SavingsEntity> savingsList;
}
