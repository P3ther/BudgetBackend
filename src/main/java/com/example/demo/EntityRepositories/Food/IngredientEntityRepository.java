package com.example.demo.EntityRepositories.Food;

import com.example.demo.Entity.Food.IngredientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IngredientEntityRepository extends JpaRepository<IngredientEntity, Long> {
    Optional<IngredientEntity> findByIdAndUserEntity_Username(Long id, String username);
    List<IngredientEntity> findAllByUserEntity_Username(String username);
}