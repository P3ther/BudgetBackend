package com.example.demo.ControllerInterface;

import com.example.demo.Dto.BillDto;
import com.example.demo.Dto.EkasaResponseDto.EkasaResponseDto;
import com.example.demo.Dto.ExpenseDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface ExpenseInterface {
    @GetMapping("/expense/{id}/")
    ExpenseDto getExpense(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/expense/time/")
    List<ExpenseDto> expensesByTime(@RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token);

    @PostMapping("/expense/time/category/{id}/")
    List<ExpenseDto> expensesByTimeAndCategory(@PathVariable("id") Long categoryId, @RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token);

    @PostMapping("/expense/")
    ExpenseDto addExpense(@RequestBody ExpenseDto expenseDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/expenses/")
    List<ExpenseDto> addMultipleExpenses(@RequestBody List<ExpenseDto> expenseDtoList, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/expense/edit/")
    ExpenseDto updateExpense(@RequestBody ExpenseDto expenseDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/expense/delete/")
    void deleteExpense(@RequestBody ExpenseDto expenseDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/expense/checkBill/")
    EkasaResponseDto getReceiptQrCodeDetails(@RequestBody BillDto bill);
}
