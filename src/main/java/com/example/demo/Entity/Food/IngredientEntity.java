package com.example.demo.Entity.Food;

import com.example.demo.Common.IngredientUnitEnum;
import com.example.demo.Entity.UserEntity;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IngredientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ingredientName;
    private BigDecimal ingredientCalories;
    private IngredientUnitEnum ingredientUnit;
    @OneToMany(mappedBy = "ingredient", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RecipeIngredientsEntity> recipeIngredientsEntity;
    @ManyToOne()
    @JoinColumn(name = "userIngredient")
    private UserEntity userEntity;
}