package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.TaskInterface;
import com.example.demo.Dto.TaskDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.TaskMapper;
import com.example.demo.Services.TaskService;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
public class Task implements TaskInterface {
    private final TaskService taskService;
    private final TaskMapper taskMapper;
    private final UserHelper userHelper;

    public Task(TaskService taskService, TaskMapper taskMapper, UserHelper userHelper) {
        this.taskService = taskService;
        this.taskMapper = taskMapper;
        this.userHelper = userHelper;
    }

    public TaskDto getTask(Long id, String token) throws EntityNotFoundException {
        return taskMapper.convertToDto(
                taskService.getTask(id, userHelper.getUser(token))
        );
    }

    public TaskDto createTask(TaskDto taskDto, String token) throws EntityNotFoundException {
        return taskMapper.convertToDto(
                taskService.createTask(
                        taskMapper.convertToEntity(taskDto, userHelper.getUser(token))
                )
        );
    }

    public TaskDto updateTask(TaskDto taskDto, String token) throws EntityNotFoundException {
        return taskMapper.convertToDto(
                taskService.updateTask(
                        taskMapper.convertToEntity(taskDto, userHelper.getUser(token)), userHelper.getUser(token))
                );
    }

    public List<TaskDto> listTasks(String token) {
        return taskMapper.convertToListDto(
                taskService.listTasks(userHelper.getUser(token))
        );
    }

    public void removeTask(Long id, String token) throws EntityNotFoundException {
        taskService.removeTask(id, userHelper.getUser(token));
    }
}
