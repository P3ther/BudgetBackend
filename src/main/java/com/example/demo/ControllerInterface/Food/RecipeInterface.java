package com.example.demo.ControllerInterface.Food;

import com.example.demo.Dto.Food.RecipeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

public interface RecipeInterface {
    @GetMapping("/recipe/{id}/")
    RecipeDto getRecipe(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/recipe/")
    RecipeDto createRecipe(@Valid @RequestBody RecipeDto recipeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PutMapping("/recipe/")
    RecipeDto updateRecipe(@Valid @RequestBody RecipeDto recipeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/recipes/")
    List<RecipeDto> listRecipes(@RequestHeader("Authorization") String token);

    @DeleteMapping("/recipe/{id}/")
    void deleteRecipe(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;
}
