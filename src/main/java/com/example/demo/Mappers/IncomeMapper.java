package com.example.demo.Mappers;

import com.example.demo.Dto.IncomeDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import com.example.demo.EntityRepositories.InvestmentEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IncomeMapper {
    private final CategoryRepository categoryRepository;
    private final UserEntityRepository userRepository;
    private final InvestmentEntityRepository investmentEntityRepository;
    private final CurrencyEntityRepository currencyEntityRepository;

    public IncomeMapper(CategoryRepository categoryRepository, UserEntityRepository userRepository, InvestmentEntityRepository investmentEntityRepository, CurrencyEntityRepository currencyEntityRepository) {
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
        this.investmentEntityRepository = investmentEntityRepository;
        this.currencyEntityRepository = currencyEntityRepository;
    }

    public IncomeDto convertToDto(IncomeEntity incomeEntity) {
        return IncomeDto.builder()
                .id(incomeEntity.getId())
                .name(incomeEntity.getName())
                .date(incomeEntity.getDate())
                .price(incomeEntity.getPrice())
                .addressToReceipt(incomeEntity.getAddressToReceipt())
                .investmentId(incomeEntity.getInvestmentIncome() == null ? -1 : incomeEntity.getInvestmentIncome().getId())
                .category(incomeEntity.getCategoryIncome() == null ? -1 : incomeEntity.getCategoryIncome().getId())
                .categoryName(incomeEntity.getCategoryIncome() == null ? "" :incomeEntity.getCategoryIncome().getName())
                .currencyId(incomeEntity.getIncomeCurrency() == null ? -1 : incomeEntity.getIncomeCurrency().getId())
                .currencySign(incomeEntity.getIncomeCurrency() == null ? "" :incomeEntity.getIncomeCurrency().getSign())
                .build();
    }

    public IncomeEntity convertToEntity(IncomeDto incomeDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        InvestmentEntity investmentEntity;
        CurrencyEntity currencyEntity;
        if(incomeDto.getInvestmentId() != null) {
            investmentEntity = investmentEntityRepository.findById(incomeDto.getInvestmentId()).orElse(null);
        } else {
            investmentEntity = null;
        }
        if(incomeDto.getCurrencyId() != null) {
            currencyEntity = currencyEntityRepository.findById(incomeDto.getCurrencyId()).orElse(null);
        } else {
            currencyEntity = null;
        }

        return IncomeEntity.builder()
                .id(incomeDto.getId())
                .name(incomeDto.getName())
                .date(incomeDto.getDate())
                .price(incomeDto.getPrice())
                .addressToReceipt(incomeDto.getAddressToReceipt())
                .incomeCurrency(currencyEntity)
                .investmentIncome(investmentEntity)
                .categoryIncome(incomeDto.getCategory() == null ?
                                null
                                :
                                categoryRepository.findById(incomeDto.getCategory()).orElse(null)
                )
                .userWithAccessIncome(userEntity)
                .build();
    }

    public List<IncomeDto> convertToListDto(List<IncomeEntity> incomeEntities) {
        List<IncomeDto> incomeDtoList = new ArrayList<>();
        for (IncomeEntity incomeEntity : incomeEntities) {
            incomeDtoList.add(convertToDto(incomeEntity));
        }
        return incomeDtoList;
    }
}
