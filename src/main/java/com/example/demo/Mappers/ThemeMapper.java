package com.example.demo.Mappers;

import com.example.demo.Dto.ThemeDto;
import com.example.demo.Entity.ThemeEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ThemeMapper {

    public ThemeDto convertToDto(ThemeEntity themeEntity) {
        return ThemeDto
                .builder()
                .id(themeEntity.getId())
                .themeName(themeEntity.getThemeName())
                .themeFilename(themeEntity.getThemeFilename())
                .build();
    }

    public ThemeEntity convertToEntity(ThemeDto themeDto) {
        return ThemeEntity
                .builder()
                .id(themeDto.getId())
                .themeName(themeDto.getThemeName())
                .themeFilename(themeDto.getThemeFilename())
                .build();
    }

    public List<ThemeDto> convertToListDto(List<ThemeEntity> themeEntityList) {
        List<ThemeDto> themeDtoList = new ArrayList<>();
        for(ThemeEntity themeEntity : themeEntityList) {
            themeDtoList.add(convertToDto(themeEntity));
        }
        return themeDtoList;
    }

    public List<ThemeEntity> convertToListEntity(List<ThemeDto> themeDtoList) {
        List<ThemeEntity> themeEntityList = new ArrayList<>();
        for(ThemeDto themeDto : themeDtoList) {
            themeEntityList.add(convertToEntity(themeDto));
        }
        return themeEntityList;
    }
}
