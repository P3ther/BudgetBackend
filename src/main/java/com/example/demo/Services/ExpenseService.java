package com.example.demo.Services;

import com.example.demo.Dto.BillDto;
import com.example.demo.Dto.EkasaResponseDto.EkasaResponseDto;
import com.example.demo.Dto.EkasaResponseDto.ReceiptDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ExpenseService {
    private final ExpenseRepository expenseRepository;
    private final UserEntityRepository userRepository;

    @Autowired
    public ExpenseService(ExpenseRepository expenseRepository, UserEntityRepository userRepository) {
        this.expenseRepository = expenseRepository;
        this.userRepository = userRepository;
    }

    public ExpenseEntity getExpense(Long id, String user) throws EntityNotFoundException {
        return expenseRepository.findByIdAndUserWithAccessExpense_Username(id, user).orElseThrow(EntityNotFoundException::new);
    }

    public List<ExpenseEntity> expensesByTime(StartEndTimeDto startEndTimeDto, String user) {
        return expenseRepository.findAllByDateBetweenAndInvestmentExpenseIsNullAndUserWithAccessExpense_Username(
                startEndTimeDto.getStartDateTime(),
                startEndTimeDto.getEndDateTime(),
                user
        );
    }

    public List<ExpenseEntity> expensesByTimeAndCategory(Long categoryId, StartEndTimeDto startEndTimeDto, String user) {
        return expenseRepository.findAllByDateBetweenAndCategoryExpense_IdAndInvestmentExpenseIsNullAndUserWithAccessExpense_Username(
                startEndTimeDto.getStartDateTime(),
                startEndTimeDto.getEndDateTime(),
                categoryId,
                user
        );
    }

    public ExpenseEntity addExpense(ExpenseEntity expenseEntity, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        expenseEntity.setUserWithAccessExpense(userEntity);
        return expenseRepository.save(expenseEntity);
    }

    public List<ExpenseEntity> addExpenses(List<ExpenseEntity> expenseEntities, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        for(ExpenseEntity expenseEntity : expenseEntities) {
            expenseEntity.setUserWithAccessExpense(userEntity);
        }
        return expenseRepository.saveAll(expenseEntities);
    }

    public ExpenseEntity updateExpense(ExpenseEntity expenseEntity, String user) throws EntityNotFoundException {
        ExpenseEntity expenseEntityFound = expenseRepository.findByIdAndUserWithAccessExpense_Username(expenseEntity.getId(), user).orElseThrow(EntityNotFoundException::new);
        expenseEntityFound.setName(expenseEntity.getName());
        expenseEntityFound.setDate(expenseEntity.getDate());
        expenseEntityFound.setPrice(expenseEntity.getPrice());
        expenseEntityFound.setAddressToReceipt(expenseEntity.getAddressToReceipt());
        expenseEntityFound.setCategoryExpense(expenseEntity.getCategoryExpense());
        expenseEntityFound.setExpenseCurrency(expenseEntity.getExpenseCurrency());
        return expenseRepository.save(expenseEntityFound);
    }

    public void deleteExpense(ExpenseEntity expenseEntity, String user) throws EntityNotFoundException {
        ExpenseEntity expenseEntityFound = expenseRepository.findByIdAndUserWithAccessExpense_Username(expenseEntity.getId(), user).orElseThrow(EntityNotFoundException::new);
        expenseRepository.delete(expenseEntityFound);
    }


    public EkasaResponseDto getReceiptQrCodeDetails(BillDto bill) {
        URI uri = URI.create("https://ekasa.financnasprava.sk/mdu/api/v1/opd/receipt/find");

        RestTemplate restTemplate = new RestTemplate();
        EkasaResponseDto responseDto = restTemplate.postForObject(uri, bill, EkasaResponseDto.class);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        ReceiptDto receiptDto = responseDto.getReceipt();
        receiptDto.setDate(LocalDateTime.parse(receiptDto.getCreateDate(), formatter));
        return responseDto;
    }
}
