package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExpenseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private LocalDateTime date;
    private BigDecimal price;
    private String addressToReceipt;
    @ManyToOne()
    @JoinColumn(name = "categoryId")
    private CategoryEntity categoryExpense;
    @ManyToOne()
    @JoinColumn(name = "expenseCurrencyId")
    private CurrencyEntity expenseCurrency;
    // Should be one to one!
    @ManyToOne()
    @JoinColumn(name = "investmentId")
    private InvestmentEntity investmentExpense;
    @ManyToOne()
    @JoinColumn(name = "userWithAccessExpense")
    private UserEntity userWithAccessExpense;
}
