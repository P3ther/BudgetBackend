package com.example.demo.ControllerInterface;

import com.example.demo.Dto.NoteDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface NoteInterface {

    @GetMapping("/note/{id}/")
    NoteDto getNote(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/note/")
    NoteDto createNote(@RequestBody NoteDto noteDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PutMapping("/note/")
    NoteDto updateNote(@RequestBody NoteDto noteDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/notes/")
    List<NoteDto> listNotes(@RequestHeader("Authorization") String token);

    @DeleteMapping("/note/{id}/")
    void deleteNote(@PathVariable("id") Long id,  @RequestHeader("Authorization") String token) throws EntityNotFoundException;
}
