package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CurrencyPairsDto {
    private Long id;
    private Long firstCurrencyId;
    private String firstCurrencyCode;
    private String firstCurrencyName;
    private String firstCurrencySign;
    private Long secondCurrencyId;
    private String secondCurrencyCode;
    private String secondCurrencyName;
    private String secondCurrencySign;
    @NotBlank
    private BigDecimal exchangeRate;
    @NotBlank
    private LocalDateTime lastChange;
}
