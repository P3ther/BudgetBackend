package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SavingsDto {
    private Long id;
    private BigDecimal value;
    private BigDecimal savedValue;
    private LocalDateTime date;
    private CurrencyDto currency;
}
