package com.example.demo.ControllerInterface;

import com.example.demo.Dto.GraphValueDto;
import com.example.demo.Dto.StartEndTimeDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

public interface GraphInterface {
    @PostMapping("/graph/expenses/")
    List<GraphValueDto> getGraphExpenses(@RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token);
}
