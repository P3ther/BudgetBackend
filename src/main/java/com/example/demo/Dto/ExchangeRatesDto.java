package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRatesDto {
    private String result;
    private String documentation;
    private String terms_of_use;
    private Timestamp time_last_update_unix;
    private String time_last_update_utc;
    private Timestamp time_next_update_unix;
    private String time_next_update_utc;
    private String base_code;
    private Map<String, BigDecimal> conversion_rates;
}