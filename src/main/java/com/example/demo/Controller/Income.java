package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.IncomeInterface;
import com.example.demo.Dto.IncomeDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.IncomeMapper;
import com.example.demo.Services.IncomeService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Income implements IncomeInterface {
    private final IncomeService incomeService;
    private final IncomeMapper incomeMapper;
    private final UserHelper userHelper;

    public Income(IncomeService incomeService, IncomeMapper incomeMapper, UserHelper userHelper) {
        this.incomeService = incomeService;
        this.incomeMapper = incomeMapper;
        this.userHelper = userHelper;
    }

    public IncomeDto getIncome(Long id, String token) throws EntityNotFoundException {
        return incomeMapper.convertToDto(incomeService.getIncome(id, userHelper.getUser(token)));
    }

    public List<IncomeDto> incomesByTime(StartEndTimeDto startEndTimeDto, String token) {
        return  incomeMapper.convertToListDto(
                incomeService.incomesByTime(startEndTimeDto, userHelper.getUser(token))
        );
    }

    public List<IncomeDto> incomesByTimeAndCategory(Long categoryId, StartEndTimeDto startEndTimeDto, String token) {
        return  incomeMapper.convertToListDto(
                incomeService.incomesByTimeAndCategory(categoryId, startEndTimeDto, userHelper.getUser(token))
        );
    }

    public IncomeDto addIncome(IncomeDto incomeDto, String token) throws EntityNotFoundException {
        return incomeMapper.convertToDto(
          incomeService.addIncome(
                  incomeMapper.convertToEntity(incomeDto, userHelper.getUser(token))
          )
        );
    }

    public IncomeDto updateIncome(IncomeDto incomeDto, String token) throws EntityNotFoundException {
        return incomeMapper.convertToDto(
                incomeService.updateIncome(
                        incomeMapper.convertToEntity(incomeDto, userHelper.getUser(token)))
        );
    }

    public void deleteIncome(IncomeDto incomeDto, String token) throws EntityNotFoundException {
        incomeService.deleteIncome(
                incomeMapper.convertToEntity(incomeDto, userHelper.getUser(token))
        );
    }
}
