package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.GraphInterface;
import com.example.demo.Dto.GraphValueDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Services.GraphService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Graph implements GraphInterface {
    private final GraphService graphService;
    private final UserHelper userHelper;

    public Graph(GraphService graphService, UserHelper userHelper) {
        this.graphService = graphService;
        this.userHelper = userHelper;
    }

    public List<GraphValueDto> getGraphExpenses(StartEndTimeDto startEndTimeDto, String token) {
        return graphService.getExpensesGraphValues(startEndTimeDto, userHelper.getUser(token));
    }
}
