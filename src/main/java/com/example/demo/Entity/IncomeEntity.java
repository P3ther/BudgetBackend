package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IncomeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private LocalDateTime date;
    private BigDecimal price;
    private String addressToReceipt;
    @ManyToOne()
    @JoinColumn(name = "categoryId")
    private CategoryEntity categoryIncome;
    @ManyToOne()
    @JoinColumn(name = "incomeCurrencyId", nullable = false)
    private CurrencyEntity incomeCurrency;
    @ManyToOne()
    @JoinColumn(name = "investmentId")
    private InvestmentEntity investmentIncome;
    @ManyToOne()
    @JoinColumn(name = "userWithAccessIncome")
    private UserEntity userWithAccessIncome;
}
