package com.example.demo.EndPoints;

public class UserEndpoints {
    public static final String deleteUser = "/user/forgotPassword/";
    public static final String forgotPassword = "/user/forgotPassword/";
    public static final String verifyPasswordRegenerationCode = "/user/regenerationCode/";
    public static final String resetPassword = "/user/resetPassword/";
    public static final String userProperties = "/user/properties/";
}
