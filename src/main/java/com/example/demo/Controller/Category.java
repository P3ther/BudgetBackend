package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.CategoryInterface;
import com.example.demo.Dto.CategoryDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.CategoryMapper;
import com.example.demo.Services.CategoryService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Category implements CategoryInterface {

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final UserHelper userHelper;

    public Category(CategoryService categoryService, CategoryMapper categoryMapper, UserHelper userHelper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.userHelper = userHelper;
    }

    public CategoryDto readCategory(Long id, String token) throws EntityNotFoundException {
        return categoryMapper.convertToDto(
                categoryService.readCategory(id, userHelper.getUser(token)), userHelper.getUser(token)
        );
    }

    public CategoryDto readCategoryByTime(Long id, StartEndTimeDto startEndTimeDto, String token) throws EntityNotFoundException {
        return categoryMapper.convertToDto(
                categoryService.readCategoryByTime(id, startEndTimeDto, userHelper.getUser(token)), userHelper.getUser(token)
        );
    }

    public List<CategoryDto> getCategories(String token) throws EntityNotFoundException {
        String userName = userHelper.getUser(token);
        return categoryMapper.convertToListDto(categoryService.getCategories(userName), userName);
    }
    public List<CategoryDto> getCategoriesAndValuesForTime(StartEndTimeDto startEndTimeDto, String token) throws EntityNotFoundException {
        String userName = userHelper.getUser(token);
        return categoryMapper.convertToListDto(categoryService.getCategoriesAndValuesForTime(startEndTimeDto, userName), userName);
    }

    public List<CategoryDto> getExpenseCategories(String token) throws EntityNotFoundException {
        String userName = userHelper.getUser(token);
        return categoryMapper.convertToListDto(categoryService.getExpenseCategories(userName), userName);
    }
    public List<CategoryDto> getIncomeCategories(String token) throws EntityNotFoundException {
        String userName = userHelper.getUser(token);
        return categoryMapper.convertToListDto(
                categoryService.getIncomeCategories(userName),
                userName
        );
    }
    public CategoryDto addCategory(CategoryDto categoryDto, String token) throws EntityNotFoundException {
        String userName = userHelper.getUser(token);
        return categoryMapper.convertToDto(
                categoryService.addCategory(
                        categoryMapper.convertToEntity(categoryDto, userName
                        )
                ), userName
        );
    }

    public CategoryDto updateCategory(CategoryDto categoryDto, String token) throws EntityNotFoundException {
        String userName = userHelper.getUser(token);
        return categoryMapper.convertToDto(
                categoryService.updateCategory(
                        categoryMapper.convertToEntity(
                                categoryDto, userName
                        )
                ), userName
        );
    }

    public void deleteCategory(Long id, String token) throws EntityNotFoundException {
        categoryService.deleteCategory(id, userHelper.getUser(token));
    }

    public void deleteCategoryAndAllItems(Long id, String token) throws EntityNotFoundException {
        categoryService.deleteCategoryAndAllItems(id, userHelper.getUser(token));
    }

}
