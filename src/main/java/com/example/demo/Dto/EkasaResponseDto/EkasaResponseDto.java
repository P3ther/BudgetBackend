package com.example.demo.Dto.EkasaResponseDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EkasaResponseDto {
    private ReceiptDto receipt;
}
