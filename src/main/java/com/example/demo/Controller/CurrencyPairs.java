package com.example.demo.Controller;

import com.example.demo.ControllerInterface.CurrencyPairsInterface;
import com.example.demo.Dto.CurrencyPairsDto;
import com.example.demo.Dto.ExchangeRatesDto;
import com.example.demo.Entity.CurrencyPairsEntity;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import com.example.demo.Mappers.CurrencyPairsMapper;
import com.example.demo.Services.CurrencyPairsService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@RestController
public class CurrencyPairs implements CurrencyPairsInterface {
    private final CurrencyPairsService currencyPairsService;
    private final CurrencyPairsMapper currencyPairsMapper;
    private final CurrencyPairsEntityRepository currencyPairsEntityRepository;

    public CurrencyPairs(CurrencyPairsService currencyPairsService, CurrencyPairsMapper currencyPairsMapper, CurrencyPairsEntityRepository currencyPairsEntityRepository) {
        this.currencyPairsService = currencyPairsService;
        this.currencyPairsMapper = currencyPairsMapper;
        this.currencyPairsEntityRepository = currencyPairsEntityRepository;
    }


    public List<CurrencyPairsDto> getCurrencyPairs() {
        return currencyPairsMapper.convertToListDto(currencyPairsService.getCurrencyPairs());
    }

    public void updateExchangeRates() throws ParseException {
        currencyPairsService.updateExchangeRates();
    }
}
