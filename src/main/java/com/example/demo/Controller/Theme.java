package com.example.demo.Controller;

import com.example.demo.ControllerInterface.ThemeInterface;
import com.example.demo.Dto.ThemeDto;
import com.example.demo.Mappers.ThemeMapper;
import com.example.demo.Services.ThemeService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Theme implements ThemeInterface {
    private final ThemeService themeService;
    private final ThemeMapper themeMapper;

    public Theme(ThemeService themeService, ThemeMapper themeMapper) {
        this.themeService = themeService;
        this.themeMapper = themeMapper;
    }

    public List<ThemeDto> getThemes() {
        return themeMapper.convertToListDto(themeService.getThemes());
    }

}
