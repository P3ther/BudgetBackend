package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String noteName;
    @Column(columnDefinition = "TEXT")
    private String noteDetails;
    private LocalDateTime dateCreated;
    private LocalDateTime lastUpdated;
    @ManyToOne()
    @JoinColumn(name = "userNote")
    private UserEntity userEntity;
}
