package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.InvestmentDataInterface;
import com.example.demo.Dto.InvestmentDataDto;
import com.example.demo.Services.InvestmentDataService;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class InvestmentData implements InvestmentDataInterface {
    private final InvestmentDataService investmentDataService;
    private final UserHelper userHelper;

    public InvestmentData(InvestmentDataService investmentDataService, UserHelper userHelper) {
        this.investmentDataService = investmentDataService;
        this.userHelper = userHelper;
    }

    public List<InvestmentDataDto> getInvestmentData(
            List<LocalDateTime> dateTimes,
            String token) {
        return investmentDataService.getInvestmentData(dateTimes, userHelper.getUser(token));
    }
    public List<InvestmentDataDto> getInvestmentValue(
            List<LocalDateTime> dateTimes,
            String token) {
        return investmentDataService.getInvestmentValue(dateTimes, userHelper.getUser(token));
    }
    public List<InvestmentDataDto> getInvestmentGain(
            List<LocalDateTime> dateTimes,
            String token) {
        return investmentDataService.getInvestmentGain(dateTimes, userHelper.getUser(token));
    }

    public List<InvestmentDataDto> getInvestmentValueForPlatform(
            List<LocalDateTime> dateTimes,
            Long platformId,
            String token) {
        return investmentDataService.getInvestmentValueForPlatform(dateTimes, platformId, userHelper.getUser(token));
    }
    public List<InvestmentDataDto> getInvestmentGainForPlatform(
            List<LocalDateTime> dateTimes,
            Long platformId,
            String token) {
        return investmentDataService.getInvestmentGainForPlatform(dateTimes, platformId, userHelper.getUser(token));
    }
}
