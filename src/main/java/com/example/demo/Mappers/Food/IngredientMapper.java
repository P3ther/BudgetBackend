package com.example.demo.Mappers.Food;

import com.example.demo.Common.ModelMapperConfig;
import com.example.demo.Dto.Food.IngredientDto;
import com.example.demo.Dto.Food.RecipeDto;
import com.example.demo.Dto.Food.RecipeIngredientsDtoIngredient;
import com.example.demo.Entity.Food.IngredientEntity;
import com.example.demo.Entity.Food.RecipeIngredientsEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class IngredientMapper {
    private final UserEntityRepository userRepository;
    private final ModelMapperConfig modelMapperConfig;

    public IngredientMapper(UserEntityRepository userRepository, ModelMapperConfig modelMapperConfig) {
        this.userRepository = userRepository;
        this.modelMapperConfig = modelMapperConfig;
    }

    public IngredientDto convertToDto(IngredientEntity ingredientEntity) {
        List<RecipeIngredientsDtoIngredient> ingredientsDtoRecipesList = new ArrayList<>();
        for(RecipeIngredientsEntity recipeIngredientsEntity : ingredientEntity.getRecipeIngredientsEntity()) {
            ingredientsDtoRecipesList.add(RecipeIngredientsDtoIngredient.builder()
                    .id(recipeIngredientsEntity.getId())
                    .quantity(recipeIngredientsEntity.getQuantity())
                    .recipe(RecipeDto.builder()
                            .id(recipeIngredientsEntity.getRecipe().getId())
                            .recipeName(recipeIngredientsEntity.getRecipe().getRecipeName())
                            .recipeInstructions(recipeIngredientsEntity.getRecipe().getRecipeInstructions())
                            .lastDateTimeUsed(recipeIngredientsEntity.getRecipe().getLastDateTimeUsed())
                            .build()

                    )
                    .build());
            //recipeIngredientsEntity.getRecipe().setRecipeIngredientEntities(null);
        }

        return IngredientDto.builder()
                .id(ingredientEntity.getId())
                .ingredientName(ingredientEntity.getIngredientName())
                .ingredientCalories(ingredientEntity.getIngredientCalories())
                .ingredientUnit(ingredientEntity.getIngredientUnit())
                .recipeIngredients(ingredientsDtoRecipesList)
                .build();
    }
    public List<IngredientDto> convertToListDto(List<IngredientEntity> ingredientEntityList) {
        List<IngredientDto> ingredientDtoList = new ArrayList<>();
        for(IngredientEntity ingredientEntity : ingredientEntityList) {
            ingredientDtoList.add(convertToDto(ingredientEntity));
        }
        return ingredientDtoList;
    }
    public IngredientEntity converterToEntity(IngredientDto ingredientDto, UserEntity userEntity) {
        return IngredientEntity.builder()
                .id(ingredientDto.getId())
                .ingredientName(ingredientDto.getIngredientName())
                .ingredientCalories(ingredientDto.getIngredientCalories())
                .ingredientUnit(ingredientDto.getIngredientUnit())
                .recipeIngredientsEntity(ingredientDto.getRecipeIngredients() == null ?
                        new ArrayList<>()
                        : ingredientDto.getRecipeIngredients().stream().map(element -> modelMapperConfig.modelMapper().map(element, RecipeIngredientsEntity.class)).collect(Collectors.toList()))
                .userEntity(userEntity)
                .build();
    }
    public IngredientEntity convertToEntity(IngredientDto ingredientDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        return converterToEntity(ingredientDto, userEntity);
    }
    public List<IngredientEntity> convertToListEntity(List<IngredientDto> ingredientDtos, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<IngredientEntity> ingredientEntities = new ArrayList<>();
        for(IngredientDto ingredientDto :ingredientDtos) {
            ingredientEntities.add(converterToEntity(ingredientDto, userEntity));
        }
        return ingredientEntities;
    }

}