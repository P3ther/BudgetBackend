package com.example.demo.EntityRepositories;

import com.example.demo.Entity.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyEntityRepository extends JpaRepository<CurrencyEntity, Long>  {

    Optional<CurrencyEntity> findById(Long currencyId);
}
