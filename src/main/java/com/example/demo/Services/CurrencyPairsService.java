package com.example.demo.Services;

import com.example.demo.Dto.ExchangeRatesDto;
import com.example.demo.Entity.CurrencyEntity;
import com.example.demo.Entity.CurrencyPairsEntity;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CurrencyPairsService {
    private final CurrencyPairsEntityRepository currencyPairsEntityRepository;
    private final CurrencyEntityRepository currencyEntityRepository;

    public CurrencyPairsService(CurrencyPairsEntityRepository currencyPairsEntityRepository, CurrencyEntityRepository currencyEntityRepository) {
        this.currencyPairsEntityRepository = currencyPairsEntityRepository;
        this.currencyEntityRepository = currencyEntityRepository;
    }

    public void addNewCurrency(CurrencyEntity newCurrency) {
        List<CurrencyEntity> currencyEntityList = currencyEntityRepository.findAll();
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        List<CurrencyEntity> filteredCurrencies = currencyEntityList.stream().filter(c -> !c.getId().equals(newCurrency.getId())).collect(Collectors.toList());
        for(CurrencyEntity currencyEntity : filteredCurrencies) {
            currencyPairsEntityList.add(CurrencyPairsEntity
                    .builder()
                    .firstCurrency(newCurrency)
                    .secondCurrency(currencyEntity)
                    .build());
        }
        CurrencyPairsEntity entity = CurrencyPairsEntity.builder().id(-1L).build();
        currencyPairsEntityRepository.saveAll(currencyPairsEntityList);
    }

    public List<CurrencyPairsEntity> getCurrencyPairs() {
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        return currencyPairsEntityList;
    }

    public void updateExchangeRates() throws ParseException {
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsEntityRepository.findAll();
        ExchangeRatesDto exchangeRatesDto = new ExchangeRatesDto();
        String uri = "https://v6.exchangerate-api.com/v6/4beea8faa7ff34a4b9c88aa4/latest/";
        String currencyCode = "";
        RestTemplate restTemplate = new RestTemplate();
        for (int i = 0, currencyPairsEntityListSize = currencyPairsEntityList.size(); i < currencyPairsEntityListSize; i++) {
            if (currencyCode.equals(currencyPairsEntityList.get(i).getFirstCurrency().getCurrencyCode())) {
                int finalI = i;
                exchangeRatesDto.getConversion_rates().forEach((a, b) -> {
                    if (a.equals(currencyPairsEntityList.get(finalI).getSecondCurrency().getCurrencyCode()))
                        currencyPairsEntityList.get(finalI).setExchangeRate(b);
                });
                DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
                Date date1 = formatter.parse(exchangeRatesDto.getTime_last_update_utc());
                LocalDateTime localDateTime = date1.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
                currencyPairsEntityList.get(finalI).setLastChange(localDateTime);
            } else {
                currencyCode = currencyPairsEntityList.get(i).getFirstCurrency().getCurrencyCode();
                exchangeRatesDto = restTemplate.getForObject(uri + currencyCode, ExchangeRatesDto.class);
                assert exchangeRatesDto != null;
                int finalI1 = i;
                exchangeRatesDto.getConversion_rates().forEach((a, b) -> {
                    if (a.equals(currencyPairsEntityList.get(finalI1).getSecondCurrency().getCurrencyCode()))
                        currencyPairsEntityList.get(finalI1).setExchangeRate(b);
                });
                DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
                Date date1 = formatter.parse(exchangeRatesDto.getTime_last_update_utc());
                LocalDateTime localDateTime = date1.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
                currencyPairsEntityList.get(finalI1).setLastChange(localDateTime);
            }
        }
        currencyPairsEntityRepository.saveAll(currencyPairsEntityList);
    }
}
