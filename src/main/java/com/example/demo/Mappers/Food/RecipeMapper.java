package com.example.demo.Mappers.Food;

import com.example.demo.Common.ModelMapperConfig;
import com.example.demo.Common.RecipeHelper;
import com.example.demo.Dto.Food.IngredientDto;
import com.example.demo.Dto.Food.RecipeDto;
import com.example.demo.Dto.Food.RecipeIngredientsDtoRecipe;
import com.example.demo.Entity.Food.RecipeEntity;
import com.example.demo.Entity.Food.RecipeIngredientsEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RecipeMapper {
    private final UserEntityRepository userRepository;
    private final RecipeHelper recipeHelper;
    private final ModelMapperConfig modelMapperConfig;

    public RecipeMapper(UserEntityRepository userRepository, RecipeHelper recipeHelper, ModelMapperConfig modelMapperConfig) {
        this.userRepository = userRepository;
        this.recipeHelper = recipeHelper;
        this.modelMapperConfig = modelMapperConfig;
    }

    public RecipeDto convertToDto(RecipeEntity recipeEntity) {
        List<RecipeIngredientsDtoRecipe> recipeIngredientsDtoRecipes = new ArrayList<>();
        for(RecipeIngredientsEntity recipeIngredientsEntity : recipeEntity.getRecipeIngredientEntities()) {
            recipeIngredientsDtoRecipes.add(
                    RecipeIngredientsDtoRecipe.builder()
                            .id(recipeIngredientsEntity.getId())
                            .quantity(recipeIngredientsEntity.getQuantity())
                            .ingredient(
                                    IngredientDto.builder().id(recipeIngredientsEntity.getIngredient().getId())
                                            .ingredientName(recipeIngredientsEntity.getIngredient().getIngredientName())
                                            .ingredientCalories(recipeIngredientsEntity.getIngredient().getIngredientCalories())
                                            .ingredientUnit(recipeIngredientsEntity.getIngredient().getIngredientUnit())
                                            .build()
                            )
                            .build()
            );
        }

        return RecipeDto.builder()
                .id(recipeEntity.getId())
                .recipeName(recipeEntity.getRecipeName())
                .recipeInstructions(recipeEntity.getRecipeInstructions())
                .recipeIngredients(recipeIngredientsDtoRecipes)
                .lastDateTimeUsed(recipeEntity.getLastDateTimeUsed())
                .recipeCalories(recipeHelper.getRecipeCalories(recipeEntity))
                .build();
    }
    public List<RecipeDto> convertToListDto(List<RecipeEntity> recipeEntityList) {
        List<RecipeDto> recipeDtoList = new ArrayList<>();
        for(RecipeEntity recipeEntity : recipeEntityList) {
            recipeDtoList.add(convertToDto(recipeEntity));
        }
        return recipeDtoList;
    }

    public RecipeEntity converterToEntity(RecipeDto recipeDto, UserEntity userEntity)  {
        return RecipeEntity.builder()
                .id(recipeDto.getId())
                .recipeName(recipeDto.getRecipeName())
                .recipeInstructions(recipeDto.getRecipeInstructions())
                .recipeIngredientEntities(recipeDto.getRecipeIngredients() == null ? new ArrayList<>() : recipeDto.getRecipeIngredients().stream().map(element -> modelMapperConfig.modelMapper().map(element, RecipeIngredientsEntity.class)).collect(Collectors.toList()))
                .lastDateTimeUsed(recipeDto.getLastDateTimeUsed())
                .userEntity(userEntity)
                .build();
    }
    public RecipeEntity convertToEntity(RecipeDto recipeDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        return converterToEntity(recipeDto, userEntity);
    }

    public List<RecipeEntity> convertToListEntity(List<RecipeDto> recipeDtoList, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<RecipeEntity> recipeEntityList = new ArrayList<>();
        for(RecipeDto recipeDto: recipeDtoList) {
            recipeEntityList.add(converterToEntity(recipeDto, userEntity));
        }
        return recipeEntityList;
    }
}
