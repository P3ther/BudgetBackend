package com.example.demo.Controller;

import com.example.demo.Common.CurrencyHelper;
import com.example.demo.Common.UserHelper;
import com.example.demo.Dto.BudgetDto;
import com.example.demo.Dto.DateDto;
import com.example.demo.Dto.SavingsDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.DuplicateException;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.SavingsMapper;
import com.example.demo.Services.SavingsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Savings {
    private final SavingsService savingsService;
    private final UserHelper userHelper;
    private final SavingsMapper savingsMapper;
    private final CurrencyHelper currencyHelper;
    private final Budget budgetController;

    public Savings(SavingsService savingsService, UserHelper userHelper, SavingsMapper savingsMapper, CurrencyHelper currencyHelper, Budget budgetController) {
        this.savingsService = savingsService;
        this.userHelper = userHelper;
        this.savingsMapper = savingsMapper;
        this.currencyHelper = currencyHelper;
        this.budgetController = budgetController;
    }

    @PostMapping("/savings/")
    private SavingsDto createSavings(@RequestBody SavingsDto savingsDto, @RequestHeader("Authorization") String token) throws DuplicateException, EntityNotFoundException {
        return savingsMapper.convertToDto(
                savingsService.createSavings(
                        savingsMapper.convertToEntity(savingsDto, userHelper.getUser(token)), userHelper.getUser(token)
                )
        );
    }

    @GetMapping("/savings/")
    private List<SavingsDto> listSavings(@RequestHeader("Authorization") String token) {
        return savingsMapper.convertToListDto(
                savingsService.listSavings(userHelper.getUser(token))
        );
    }

    @PostMapping("/savings/month/")
    private SavingsDto readSavingsForMonth(@RequestBody DateDto dateDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException {
        SavingsDto savingsDto = savingsMapper.convertToDto(
                savingsService.readSavingsForMonth(dateDto.getDateTime(), userHelper.getUser(token))
        );
        savingsDto.setValue(currencyHelper.convertToDefaultCurrency(userHelper.getUser(token), savingsDto.getValue(), savingsDto.getCurrency()));
        return savingsDto;
    }

    @PostMapping("/savings/range/")
    private List<SavingsDto> readSavingsForRange(@RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException {
        List<SavingsDto> savingsDtos = savingsMapper.convertToListDto(
                savingsService.readSavingsForRange(startEndTimeDto, userHelper.getUser(token))
        );
        for(SavingsDto savingsDto: savingsDtos) {
            StartEndTimeDto dateRange = StartEndTimeDto.builder().startDateTime(savingsDto.getDate().withDayOfMonth(1)).endDateTime(savingsDto.getDate().withDayOfMonth(28)).build();
            BudgetDto budgetDto = budgetController.getTimedBudget(dateRange, token);
            savingsDto.setSavedValue(budgetDto.getTotalBalance());
        }
        return savingsDtos;
    }
}
