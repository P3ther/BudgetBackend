package com.example.demo.Services.Food;

import com.example.demo.Entity.Food.RecipeEntity;
import com.example.demo.Entity.Food.RecipeIngredientsEntity;
import com.example.demo.EntityRepositories.Food.RecipeEntityRepository;
import com.example.demo.EntityRepositories.Food.RecipeIngredientsEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class RecipeService {
    private final RecipeEntityRepository recipeEntityRepository;
    private final RecipeIngredientsEntityRepository recipeIngredientsEntityRepository;

    public RecipeService(RecipeEntityRepository recipeEntityRepository, RecipeIngredientsEntityRepository recipeIngredientsEntityRepository) {
        this.recipeEntityRepository = recipeEntityRepository;
        this.recipeIngredientsEntityRepository = recipeIngredientsEntityRepository;
    }

    public RecipeEntity getRecipe(Long id, String user) throws EntityNotFoundException {
        return recipeEntityRepository.findByIdAndUserEntity_Username(id, user)
                .orElseThrow(EntityNotFoundException::new);
    }

    public RecipeEntity createRecipe(RecipeEntity newRecipe) {
        RecipeEntity newRecipeStored = recipeEntityRepository.save(newRecipe);
        for(RecipeIngredientsEntity recipeIngredientsEntity: newRecipeStored.getRecipeIngredientEntities()) {
            recipeIngredientsEntity.setRecipe(newRecipeStored);
        }
        recipeIngredientsEntityRepository.saveAll(newRecipe.getRecipeIngredientEntities());
        return newRecipeStored;
    }

    public RecipeEntity updateRecipe(RecipeEntity recipeEntity) throws EntityNotFoundException {
        RecipeEntity editedRecipe = recipeEntityRepository
                .findByIdAndUserEntity_Username(recipeEntity.getId(), recipeEntity.getUserEntity().getUsername())
                .orElseThrow(EntityNotFoundException::new);
        List<Long> idOfIngredients = new ArrayList<>();
        for(RecipeIngredientsEntity recipeIngredientsEntity : recipeEntity.getRecipeIngredientEntities()) {
            if(recipeIngredientsEntity.getId() != null) {
                idOfIngredients.add(recipeIngredientsEntity.getId());
            } else {
                recipeIngredientsEntity.setRecipe(recipeEntity);
                RecipeIngredientsEntity savedEntity = recipeIngredientsEntityRepository.save(recipeIngredientsEntity);
                idOfIngredients.add(savedEntity.getId());
            }
        }
        List<RecipeIngredientsEntity> recipeIngredientsEntities = recipeIngredientsEntityRepository.findByIdIn(idOfIngredients);
        /* For loop updates quantity on already existing Recipe Ingredients */
        for(RecipeIngredientsEntity recipeIngredientsEntity : recipeEntity.getRecipeIngredientEntities()) {
            if (recipeIngredientsEntities.stream().anyMatch(c -> Objects.equals(c.getId(), recipeIngredientsEntity.getId()))) {
                RecipeIngredientsEntity filteredEntity = recipeIngredientsEntities.stream().filter(c -> Objects.equals(c.getId(), recipeIngredientsEntity.getId())).findFirst().orElseThrow(EntityNotFoundException::new);
                int keyIndex = recipeIngredientsEntities.indexOf(filteredEntity);
                filteredEntity.setQuantity(recipeIngredientsEntity.getQuantity());
                recipeIngredientsEntities.set(keyIndex, filteredEntity);
            }
        }
        editedRecipe.setRecipeName(recipeEntity.getRecipeName());
        editedRecipe.setRecipeInstructions(recipeEntity.getRecipeInstructions());
        editedRecipe.setRecipeIngredientEntities(recipeIngredientsEntities);
        editedRecipe.setLastDateTimeUsed(recipeEntity.getLastDateTimeUsed());
        /*
            Add setter to all fields
         */
        return recipeEntityRepository.save(editedRecipe);
    }

    public List<RecipeEntity> listRecipes(String user) {
        return recipeEntityRepository.findAllByUserEntity_Username(user);
    }

    public void deleteRecipe(Long id, String user) throws EntityNotFoundException {
        RecipeEntity deletedRecipe = recipeEntityRepository.findByIdAndUserEntity_Username(id, user).orElseThrow(EntityNotFoundException::new);
        recipeEntityRepository.delete(deletedRecipe);
    }
}
