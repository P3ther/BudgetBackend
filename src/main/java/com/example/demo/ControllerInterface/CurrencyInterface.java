package com.example.demo.ControllerInterface;

import com.example.demo.Dto.CurrencyDto;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.ParseException;
import java.util.List;

public interface CurrencyInterface {
    @GetMapping("/currency/")
    List<CurrencyDto> getCurrencies();

    @PostMapping("/currency/")
    List<CurrencyDto> addNewCurrency(@RequestBody CurrencyDto currencyDto);

    @PostMapping("/currency/edit/")
    List<CurrencyDto> editCurrency(@RequestBody CurrencyDto currencyDto) throws ParseException;
}
