package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvestmentPlatformEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String name;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "investmentPlatform")
    private List<InvestmentEntity> investmentsOnPlatform;
    @ManyToOne()
    @JoinColumn(name = "userWithAccessInvestmentPlatform")
    private UserEntity userWithAccessInvestmentPlatform;
}
