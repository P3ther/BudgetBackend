package com.example.demo.Mappers;

import com.example.demo.Dto.NoteDto;
import com.example.demo.Entity.NoteEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class NoteMapper {
    private final UserEntityRepository userRepository;

    public NoteMapper(UserEntityRepository userRepository) {
        this.userRepository = userRepository;
    }

    public NoteDto convertToDto(NoteEntity noteEntity){
        return NoteDto.builder()
                .id(noteEntity.getId())
                .noteName(noteEntity.getNoteName())
                .noteDetails(noteEntity.getNoteDetails())
                .dateCreated(noteEntity.getDateCreated())
                .lastUpdated(noteEntity.getLastUpdated())
                .build();
    }
    public List<NoteDto> convertToListDto(List<NoteEntity> noteEntityList) {
        List<NoteDto> noteDtoList = new ArrayList<>();
        for(NoteEntity noteEntity: noteEntityList) {
            noteDtoList.add(convertToDto(noteEntity));
        }
        return noteDtoList;
    }

    private NoteEntity converterToEntity(NoteDto noteDto, UserEntity userEntity) {
        return NoteEntity.builder()
                .id(noteDto.getId())
                .noteName(noteDto.getNoteName())
                .noteDetails(noteDto.getNoteDetails())
                .dateCreated(noteDto.getDateCreated())
                .lastUpdated(noteDto.getLastUpdated())
                .userEntity(userEntity)
                .build();
    }

    public NoteEntity convertToEntity(NoteDto noteDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        return converterToEntity(noteDto, userEntity);
    }

    public List<NoteEntity> convertToListEntity(List<NoteDto> noteDtoList, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        List<NoteEntity> noteEntityList = new ArrayList<>();
        for(NoteDto noteDto : noteDtoList) {
            noteEntityList.add(converterToEntity(noteDto, userEntity));
        }
        return noteEntityList;
    }
}
