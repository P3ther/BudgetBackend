package com.example.demo.Controller.Food;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.Food.IngredientInterface;
import com.example.demo.Dto.Food.IngredientDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.Food.IngredientMapper;
import com.example.demo.Services.Food.IngredientService;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.List;

@RestController
public class Ingredient implements IngredientInterface {
    private final IngredientService ingredientService;
    private final IngredientMapper ingredientMapper;
    private final UserHelper userHelper;

    public Ingredient(IngredientService ingredientService, IngredientMapper ingredientMapper, UserHelper userHelper) {
        this.ingredientService = ingredientService;
        this.ingredientMapper = ingredientMapper;
        this.userHelper = userHelper;
    }

    public IngredientDto getIngredient(Long id, String token) throws EntityNotFoundException {
        return ingredientMapper.convertToDto(
                ingredientService.getIngredient(id, userHelper.getUser(token))
        );
    }

    public IngredientDto createIngredient(IngredientDto ingredientDto, String token) throws EntityNotFoundException {
        return ingredientMapper.convertToDto(
                ingredientService.createIngredient(ingredientMapper.convertToEntity(ingredientDto, userHelper.getUser(token)))
        );
    }

    public IngredientDto updateIngredient(IngredientDto ingredientDto, String token) throws EntityNotFoundException {
        return ingredientMapper.convertToDto(
                ingredientService.updateIngredient(ingredientMapper.convertToEntity(ingredientDto, userHelper.getUser(token)))
        );
    }

    public List<IngredientDto> listIngredient(String token) {
        return ingredientMapper.convertToListDto(
                ingredientService.listIngredient(userHelper.getUser(token))
        );
    }

    public void deleteIngredient(Long id, String token) throws EntityNotFoundException {
        ingredientService.deleteIngredient(id, userHelper.getUser(token));
    }
}