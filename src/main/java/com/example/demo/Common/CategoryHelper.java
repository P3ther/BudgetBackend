package com.example.demo.Common;

import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryHelper {

    private final CurrencyPairsEntityRepository currencyPairsEntityRepository;

    public CategoryHelper(CurrencyPairsEntityRepository currencyPairsEntityRepository) {
        this.currencyPairsEntityRepository = currencyPairsEntityRepository;
    }

    public BigDecimal getCategoryActualValue(CategoryEntity categoryEntity, UserEntity userEntity){
        BigDecimal result = BigDecimal.valueOf(0L);
        List<CurrencyPairsEntity> currencyPairsEntities = currencyPairsEntityRepository.findAll();
        List<CurrencyPairsEntity> currencyPairsContainDefaultCurrency = currencyPairsEntities.stream().filter(
                c -> c.getFirstCurrency().equals(categoryEntity.getCategoryCurrency())
                        ||
                        c.getSecondCurrency().equals(categoryEntity.getCategoryCurrency()))
                .collect(Collectors.toList());
        if(categoryEntity.getType() == 0) {
            //This is expense
            List<ExpenseEntity> expenseEntities = categoryEntity.getExpenseList()
                    .stream()
                    .filter(c -> !c.getExpenseCurrency().equals(categoryEntity.getCategoryCurrency()))
                    .collect(Collectors.toList());
            for(ExpenseEntity expenseEntity : categoryEntity.getExpenseList()) {
                if(expenseEntity.getExpenseCurrency().equals(categoryEntity.getCategoryCurrency())) {
                    result = result.add(expenseEntity.getPrice());
                } else {
                    for(CurrencyPairsEntity currencyPairsEntity : currencyPairsContainDefaultCurrency) {
                        if (currencyPairsEntity.getFirstCurrency().equals(expenseEntity.getExpenseCurrency())) {
                            result = result.add(
                                    expenseEntity.getPrice().multiply(currencyPairsEntity.getExchangeRate()
                                    ));
                            //System.out.println(result);
                        } else if (currencyPairsEntity.getSecondCurrency().equals(expenseEntity.getExpenseCurrency())) {
                            BigDecimal tempExchangeRate = BigDecimal.valueOf(1L).divide(currencyPairsEntity.getExchangeRate(), 2, RoundingMode.HALF_EVEN);
                            result = result.add(
                                    expenseEntity.getPrice().multiply(tempExchangeRate)
                            );
                        }
                    }
                }
            }
        } else if(categoryEntity.getType() == 1) {
            //This is income
            List<IncomeEntity> incomeEntities = categoryEntity.getIncomeList()
                    .stream()
                    .filter(c -> !c.getIncomeCurrency().equals(categoryEntity.getCategoryCurrency()))
                    .collect(Collectors.toList());
            for (IncomeEntity incomeEntity : categoryEntity.getIncomeList()) {
                if(incomeEntity.getIncomeCurrency().equals(categoryEntity.getCategoryCurrency())) {
                    result = result.add(incomeEntity.getPrice());
                } else {
                    for(CurrencyPairsEntity currencyPairsEntity : currencyPairsContainDefaultCurrency) {
                        if (currencyPairsEntity.getFirstCurrency().equals(incomeEntity.getIncomeCurrency())) {
                            result = result.add(
                                    incomeEntity.getPrice().multiply(currencyPairsEntity.getExchangeRate())
                            );
                        } else if (currencyPairsEntity.getSecondCurrency().equals(incomeEntity.getIncomeCurrency())) {
                            result = result.add(
                                    BigDecimal.valueOf(1L).divide(incomeEntity.getPrice(),2, RoundingMode.HALF_EVEN)//.setScale(2,  RoundingMode.HALF_EVEN)
                            );
                        }

                    }
                }
            }
        }
        return result;
    }
}
