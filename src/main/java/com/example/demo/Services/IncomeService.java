package com.example.demo.Services;

import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IncomeService {
    private final IncomeRepository incomeRepository;

    public IncomeService(IncomeRepository incomeRepository) {
        this.incomeRepository = incomeRepository;
    }
    public IncomeEntity getIncome(Long id, String user) throws EntityNotFoundException {
        return incomeRepository.findByIdAndUserWithAccessIncome_Username(id, user).orElseThrow(EntityNotFoundException::new);
    }

    public List<IncomeEntity> incomesByTime(StartEndTimeDto startEndTimeDto, String user) {
        return incomeRepository.findAllByDateBetweenAndUserWithAccessIncome_UsernameOrderByDateDesc(
                startEndTimeDto.getStartDateTime(),
                startEndTimeDto.getEndDateTime(),
                user
        );
    }

    public List<IncomeEntity> incomesByTimeAndCategory(Long categoryId, StartEndTimeDto startEndTimeDto, String user) {
        return incomeRepository.findAllByDateBetweenAndCategoryIncome_IdAndUserWithAccessIncome_Username(
                startEndTimeDto.getStartDateTime(),
                startEndTimeDto.getEndDateTime(),
                categoryId,
                user
        );
    }

    public IncomeEntity addIncome(IncomeEntity incomeEntity) {
        return incomeRepository.save(incomeEntity);
    }

    public IncomeEntity updateIncome(IncomeEntity incomeEntity) throws EntityNotFoundException {
        IncomeEntity incomeEntityFound = incomeRepository.findByIdAndUserWithAccessIncome_Username(
                incomeEntity.getId(),
                incomeEntity.getUserWithAccessIncome().getUsername()
        ).orElseThrow(EntityNotFoundException::new);
        incomeEntityFound.setName(incomeEntity.getName());
        incomeEntityFound.setDate(incomeEntity.getDate());
        incomeEntityFound.setPrice(incomeEntity.getPrice());
        incomeEntityFound.setAddressToReceipt(incomeEntity.getAddressToReceipt());
        incomeEntityFound.setCategoryIncome(incomeEntity.getCategoryIncome());
        incomeEntityFound.setIncomeCurrency(incomeEntity.getIncomeCurrency());
        return incomeRepository.save(incomeEntityFound);
    }

    public void deleteIncome(IncomeEntity incomeEntity) throws EntityNotFoundException {
        IncomeEntity incomeEntityFound = incomeRepository.findByIdAndUserWithAccessIncome_Username(
                incomeEntity.getId(),
                incomeEntity.getUserWithAccessIncome().getUsername()
        ).orElseThrow(EntityNotFoundException::new);
        incomeRepository.delete(incomeEntityFound);
    }
}
