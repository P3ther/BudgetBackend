package com.example.demo.ControllerInterface;

import com.example.demo.Dto.InvestmentDataDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.time.LocalDateTime;
import java.util.List;

public interface InvestmentDataInterface {
    @PostMapping("/investmentData/")
    List<InvestmentDataDto> getInvestmentData(
            @RequestBody List<LocalDateTime> dateTimes,
            @RequestHeader("Authorization") String token
    );

    @PostMapping("/investmentData/value/")
    List<InvestmentDataDto> getInvestmentValue(
            @RequestBody List<LocalDateTime> dateTimes,
            @RequestHeader("Authorization") String token
    );

    @PostMapping("/investmentData/gain/")
    List<InvestmentDataDto> getInvestmentGain(
            @RequestBody List<LocalDateTime> dateTimes
            , @RequestHeader("Authorization") String token
    );

    @PostMapping("/investmentData/value/{platformId}/")
    List<InvestmentDataDto> getInvestmentValueForPlatform(
            @RequestBody List<LocalDateTime> dateTimes,
            @PathVariable("platformId") Long platformId,
            @RequestHeader("Authorization") String token
    );

    @PostMapping("/investmentData/gain/{platformId}/")
    List<InvestmentDataDto> getInvestmentGainForPlatform(
            @RequestBody List<LocalDateTime> dateTimes,
            @PathVariable("platformId") Long platformId,
            @RequestHeader("Authorization") String token);
}
