package com.example.demo.Services.Food;

import com.example.demo.Entity.Food.IngredientEntity;
import com.example.demo.EntityRepositories.Food.IngredientEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;

@Service
public class IngredientService {
    private final IngredientEntityRepository ingredientRepository;

    public IngredientService(IngredientEntityRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public IngredientEntity getIngredient(Long id, String user) throws EntityNotFoundException {
        return ingredientRepository.findByIdAndUserEntity_Username(id, user)
                .orElseThrow(EntityNotFoundException::new);
    }

    public IngredientEntity createIngredient(IngredientEntity ingredientEntity) {
        return ingredientRepository.save(ingredientEntity);
    }

    public IngredientEntity updateIngredient(IngredientEntity ingredientEntity) throws EntityNotFoundException {
        IngredientEntity editedIngredient =  ingredientRepository.findByIdAndUserEntity_Username(ingredientEntity.getId(), ingredientEntity.getUserEntity().getUsername())
                .orElseThrow(EntityNotFoundException::new);
        editedIngredient.setIngredientName(ingredientEntity.getIngredientName());
        editedIngredient.setIngredientCalories(ingredientEntity.getIngredientCalories());
        editedIngredient.setIngredientUnit(ingredientEntity.getIngredientUnit());
        return ingredientRepository.save(editedIngredient);
    }

    public List<IngredientEntity> listIngredient(String user) {
        return ingredientRepository.findAllByUserEntity_Username(user);
    }

    public void deleteIngredient(Long id, String user) throws EntityNotFoundException {
        IngredientEntity ingredientEntity = ingredientRepository.findByIdAndUserEntity_Username(id, user).orElseThrow(EntityNotFoundException::new);
        if(ingredientEntity.getRecipeIngredientsEntity().size() < 1 ) {
            ingredientRepository.delete(ingredientEntity);
        } else {
            throw new EntityNotFoundException();
        }
    }

}