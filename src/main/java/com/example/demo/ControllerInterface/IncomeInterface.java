package com.example.demo.ControllerInterface;

import com.example.demo.Dto.IncomeDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface IncomeInterface {
    @GetMapping("/income/{id}/")
    IncomeDto getIncome(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/income/time/")
    List<IncomeDto> incomesByTime(@RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token);

    @PostMapping("/income/time/category/{id}/")
    List<IncomeDto> incomesByTimeAndCategory(@PathVariable("id") Long categoryId, @RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token);

    @PostMapping("/income/")
    IncomeDto addIncome(@RequestBody IncomeDto incomeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/income/edit/")
    IncomeDto updateIncome(@RequestBody IncomeDto incomeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/income/delete/")
    void deleteIncome(@RequestBody IncomeDto incomeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;
}
