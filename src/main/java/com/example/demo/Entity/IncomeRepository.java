package com.example.demo.Entity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface IncomeRepository extends JpaRepository<IncomeEntity, Long> {
    Optional<IncomeEntity> findByIdAndUserWithAccessIncome_Username(Long id, String userName);
    List<IncomeEntity> findAllByDateBetweenAndCategoryIncome_IdAndUserWithAccessIncome_Username(LocalDateTime startDate, LocalDateTime endTime, Long categoryId, String userName);
    List<IncomeEntity> findAllByDateBetweenAndUserWithAccessIncome_UsernameOrderByDateDesc(LocalDateTime startDate, LocalDateTime endTime, String userName);

}
