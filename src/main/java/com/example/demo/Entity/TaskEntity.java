package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String taskName;
    private String taskDetails;
    private LocalDateTime dateCreated;
    private LocalDateTime dateExpired;
    private Integer priority;
    @ManyToOne()
    @JoinColumn(name = "userTask")
    private UserEntity userEntity;
}
