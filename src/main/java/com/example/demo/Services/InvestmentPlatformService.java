package com.example.demo.Services;

import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.InvestmentEntityRepository;
import com.example.demo.EntityRepositories.InvestmentPlatformRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestmentPlatformService {
    private final InvestmentPlatformRepository investmentPlatformRepository;
    private final InvestmentEntityRepository investmentEntityRepository;
    private final ExpenseRepository expenseRepository;

    public InvestmentPlatformService(InvestmentPlatformRepository investmentPlatformRepository, InvestmentEntityRepository investmentEntityRepository, ExpenseRepository expenseRepository) {
        this.investmentPlatformRepository = investmentPlatformRepository;
        this.investmentEntityRepository = investmentEntityRepository;
        this.expenseRepository = expenseRepository;
    }

    public  InvestmentPlatformEntity getInvestmentPlatform(Long id, String username) throws EntityNotFoundException {
        return investmentPlatformRepository.findByIdAndUserWithAccessInvestmentPlatform_Username(id, username).orElseThrow(EntityNotFoundException::new);
    }

    public InvestmentPlatformEntity createInvestmentPlatform(InvestmentPlatformEntity investmentPlatformEntity) {
        return investmentPlatformRepository.save(investmentPlatformEntity);
    }

    public InvestmentPlatformEntity updateInvestmentPlatform(InvestmentPlatformEntity investmentPlatformEntity) throws EntityNotFoundException {
        InvestmentPlatformEntity platformEntity = investmentPlatformRepository
                .findByIdAndUserWithAccessInvestmentPlatform_Username(
                        investmentPlatformEntity.getId(),
                        investmentPlatformEntity.getUserWithAccessInvestmentPlatform().getUsername()
                ).orElseThrow(EntityNotFoundException::new);
        platformEntity.setName(investmentPlatformEntity.getName());
        return investmentPlatformRepository.save(platformEntity);
    }

    public void deleteInvestmentPlatform(Long id, String username) throws EntityNotFoundException {
        InvestmentPlatformEntity platformEntity = investmentPlatformRepository
                .findByIdAndUserWithAccessInvestmentPlatform_Username(id, username).orElseThrow(EntityNotFoundException::new);
        investmentPlatformRepository.delete(platformEntity);
    }

    public List<InvestmentPlatformEntity> listInvestmentPlatforms(String user) {
        return investmentPlatformRepository.findAllByUserWithAccessInvestmentPlatform_Username(user);
    }
}
