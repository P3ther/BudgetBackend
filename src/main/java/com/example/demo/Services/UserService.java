package com.example.demo.Services;

import com.example.demo.Common.EmailHelper;
import com.example.demo.Common.UserRoleEnum;
import com.example.demo.Dto.ChangePasswordDto;
import com.example.demo.Entity.CurrencyEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Exceptions.ForbidenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class UserService {

    private final UserEntityRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthenticationManager authenticationManager;
    private final CurrencyEntityRepository currencyRepository;
    private final EmailHelper emailHelper;
    @Autowired
    private JavaMailSender emailSender;


    public UserService(
            UserEntityRepository userRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            AuthenticationManager authenticationManager,
            CurrencyEntityRepository currencyRepository,
            EmailHelper emailHelper
    ) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authenticationManager = authenticationManager;
        this.currencyRepository = currencyRepository;
        this.emailHelper = emailHelper;
    }

    public void signUp(UserEntity userEntity) throws Exception {
        List<UserEntity> userEntityList = userRepository.findAll();
        boolean isUsernameUnique = true;
        for (UserEntity user: userEntityList) {
            if (userEntity.getUsername().equals(user.getUsername())) {
                isUsernameUnique = false;
                break;
            }
        }
        if(isUsernameUnique) {
            userEntity.setPassword(bCryptPasswordEncoder.encode(userEntity.getPassword()));
            userEntity.setRegistrationCode((long) ThreadLocalRandom.current().nextInt(10000000, 100000000));
            List<CurrencyEntity> currencyEntities = currencyRepository.findAll();
            if (currencyEntities.size() >0 ) {
                userEntity.setDefaultCurrency(currencyEntities.get(0));
            } else {
                CurrencyEntity newCurrency = CurrencyEntity.builder()
                        .name("Default Currency")
                        .currencyCode("DEF")
                        .sign("D")
                        .build();
                currencyRepository.save(newCurrency);
                userEntity.setDefaultCurrency(newCurrency);
            }
            userRepository.save(userEntity);
        } else {
            throw new Exception();
        }

    }

    public void changePassword(ChangePasswordDto changePasswordDto) throws EntityNotFoundException {
        Authentication result = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        changePasswordDto.getUsername(),
                        changePasswordDto.getPassword(),
                        new ArrayList<>())
        );
        if(result.isAuthenticated()) {
            UserEntity userEntity = userRepository.findByUsername(changePasswordDto.getUsername()).orElseThrow(EntityNotFoundException::new);
            userEntity.setPassword(bCryptPasswordEncoder.encode(changePasswordDto.getNewPassword()));
            userRepository.save(userEntity);
        }
    }

    public void generateForgotPasswordCode(UserEntity userEntity) throws EntityNotFoundException, MessagingException {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 57; // numeral '9'
        int targetStringLength = 8;
        Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        UserEntity editedUser = userRepository.findByUsername(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        if(editedUser == null) {
            editedUser = userRepository.findByUserEmail(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        }
        if(editedUser != null) {
            String content = emailHelper.generateEmailBody(userEntity.getUsername(), generatedString);
            editedUser.setForgotPasswordCode(generatedString);
            userRepository.save(editedUser);
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setFrom("peter.sirotnak@zoho.com");
            helper.setTo(editedUser.getUserEmail());
            helper.setSubject("Password Reset");
            helper.setText(content, true);
            emailSender.send(message);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public void verifyPasswordRegenerationCode(UserEntity userEntity) throws EntityNotFoundException {
        UserEntity editedUser;
        editedUser = userRepository.findByUsername(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        if(editedUser == null) {
            editedUser = userRepository.findByUserEmail(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        }
        if(!userEntity.getForgotPasswordCode().equals(editedUser.getForgotPasswordCode())) {
            throw new BadCredentialsException("Regeneration Code does not match");
        }
    }

    public void resetPassword(UserEntity userEntity) throws ForbidenException, EntityNotFoundException {
        UserEntity editedUser = userRepository.findByUsername(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        if(editedUser == null) {
            editedUser = userRepository.findByUserEmail(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        }
        if(userEntity.getForgotPasswordCode().equals(editedUser.getForgotPasswordCode())) {
            editedUser.setPassword(bCryptPasswordEncoder.encode(userEntity.getPassword()));
            editedUser.setForgotPasswordCode(null);
            userRepository.save(editedUser);
        } else {
            throw new ForbidenException("Test");
        }
    }

    public UserEntity getUserProperties(String user) throws EntityNotFoundException {
        return userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
    }

    public UserEntity updateUserProperties(UserEntity userEntity) throws EntityNotFoundException {
        UserEntity editedUser = userRepository.findByUsername(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        editedUser.setUserRole(userEntity.getUserRole() == null ? editedUser.getUserRole() : userEntity.getUserRole());
        editedUser.setDefaultCurrency(userEntity.getDefaultCurrency() == null ? editedUser.getDefaultCurrency() : userEntity.getDefaultCurrency());
        editedUser.setDefaultTheme(userEntity.getDefaultTheme() == null ? editedUser.getDefaultTheme() : userEntity.getDefaultTheme());
        return userRepository.save(editedUser);
    }

    public void deleteUser(UserEntity userEntity, String username) throws EntityNotFoundException {
        UserEntity deletedUser = userRepository.findByUsername(userEntity.getUsername()).orElseThrow(EntityNotFoundException::new);
        UserEntity adminUser = userRepository.findByUsername(username).orElseThrow(EntityNotFoundException::new);
        if(adminUser.getUserRole() == UserRoleEnum.Admin) {
            userRepository.delete(deletedUser);
        } else if(deletedUser.getUsername().equals(username)) {
            userRepository.delete(deletedUser);
        } else {
            throw new ForbidenException();
        }
    }

}
