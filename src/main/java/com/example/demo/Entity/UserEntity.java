package com.example.demo.Entity;

import com.example.demo.Common.UserRoleEnum;
import com.example.demo.Entity.Food.IngredientEntity;
import com.example.demo.Entity.Food.RecipeEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true, nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    private Long registrationCode;
    private UserRoleEnum userRole;
    @Column(unique = true, nullable = false)
    private String userEmail;
    @Size(min = 8, max = 8)
    private String forgotPasswordCode;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userWithAccessExpense")
    private List<ExpenseEntity> expenseList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userWithAccessIncome")
    private List<IncomeEntity> incomeList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userWithAccessCategory")
    private List<CategoryEntity> categoryList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userWithAccessInvestmentPlatform")
    private List<InvestmentPlatformEntity> investmentPlatformList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userWithAccessInvestment")
    private List<InvestmentEntity> investmentList;
    @ManyToOne()
    @JoinColumn(name = "defaultCurrency")
    private CurrencyEntity defaultCurrency;
    @ManyToOne()
    @JoinColumn(name = "defaultTheme")
    private ThemeEntity defaultTheme;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
    private List<TaskEntity> taskList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
    private List<RecipeEntity> recipeList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
    private List<IngredientEntity> ingredientList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
    private List<NoteEntity> noteList;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userSaving")
    private List<SavingsEntity> savingsList;
}
