package com.example.demo.EntityRepositories;

import com.example.demo.Entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TaskEntityRepository extends JpaRepository<TaskEntity, Long> {
    Optional<TaskEntity> findByIdAndUserEntity_Username(Long id, String username);
    List<TaskEntity> findAllByUserEntity_Username(String username);
}
