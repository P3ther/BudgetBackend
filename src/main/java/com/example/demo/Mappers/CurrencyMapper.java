package com.example.demo.Mappers;

import com.example.demo.Dto.CurrencyDto;
import com.example.demo.Entity.CurrencyEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CurrencyMapper {

    public CurrencyDto convertToDto(CurrencyEntity currencyEntity) {
        return CurrencyDto
                .builder()
                .id(currencyEntity.getId() != null ? currencyEntity.getId() : -1)
                .name(currencyEntity.getName())
                .currencyCode(currencyEntity.getCurrencyCode())
                .sign(currencyEntity.getSign())
                .build();
    }

    public CurrencyEntity convertToEntity(CurrencyDto currencyDto) {
        return CurrencyEntity
                .builder()
                .id(currencyDto.getId())
                .name(currencyDto.getName())
                .currencyCode(currencyDto.getCurrencyCode())
                .sign(currencyDto.getSign())
                .build();
    }

    public List<CurrencyDto> convertToListDto(List<CurrencyEntity> currencyEntities) {
        List<CurrencyDto> currencyDtoList = new ArrayList<>();
        for (CurrencyEntity currencyEntity: currencyEntities) {
            currencyDtoList.add(convertToDto(currencyEntity));
        }
        return currencyDtoList;
    }

    public List<CurrencyEntity> convertToListEntity(List<CurrencyDto> currencyDtos) {
        List<CurrencyEntity> currencyEntityList = new ArrayList<>();
        for(CurrencyDto currencyDto : currencyDtos) {
            currencyEntityList.add(convertToEntity(currencyDto));
        }
        return currencyEntityList;
    }
}
