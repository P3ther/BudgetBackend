package com.example.demo.Services;

import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Entity.SavingsEntity;
import com.example.demo.EntityRepositories.SavingsRepository;
import com.example.demo.Exceptions.DuplicateException;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.SavingsMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SavingsService {
    private final SavingsRepository savingsRepository;
    private final SavingsMapper savingsMapper;

    public SavingsService(SavingsRepository savingsRepository, SavingsMapper savingsMapper) {
        this.savingsRepository = savingsRepository;
        this.savingsMapper = savingsMapper;
    }

    public SavingsEntity createSavings(SavingsEntity savingsEntity, String username) throws DuplicateException {
        List<SavingsEntity> savingsEntityList = savingsRepository.findAllByUserSaving_Username(username);
        List<SavingsEntity> sameMonthSavings = savingsEntityList.stream().filter(c -> c.getDate().getMonth() == savingsEntity.getDate().getMonth()).collect(Collectors.toList());
        if (sameMonthSavings.size() > 0) throw new DuplicateException();
        else return savingsRepository.save(savingsEntity);
    }

    public List<SavingsEntity> listSavings(String username) {
        return savingsRepository.findAllByUserSaving_Username(username);
    }

    public SavingsEntity readSavingsForMonth(LocalDateTime dateTime, String username) throws EntityNotFoundException {
        LocalDateTime startDate = dateTime.withDayOfMonth(1);
        LocalDateTime endDate = dateTime.withDayOfMonth(28);
        System.out.println(startDate);
        System.out.println(endDate);
        return savingsRepository.findByDateBetweenAndUserSaving_Username(startDate, endDate, username).orElseThrow(EntityNotFoundException::new);
    }

    public List<SavingsEntity> readSavingsForRange(StartEndTimeDto dateRange, String username) throws EntityNotFoundException {
        LocalDateTime startDate = dateRange.getStartDateTime().withDayOfMonth(1);
        LocalDateTime endDate = dateRange.getEndDateTime().withDayOfMonth(28);
        return savingsRepository.findAllByDateBetweenAndUserSaving_Username(startDate, endDate, username);
    }
}
