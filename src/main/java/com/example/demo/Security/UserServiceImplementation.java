package com.example.demo.Security;

import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserServiceImplementation implements UserDetailsService {
    private final UserEntityRepository userEntityRepository;

    public UserServiceImplementation(UserEntityRepository userEntityRepository) {
        this.userEntityRepository = userEntityRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = null;
        try {
            userEntity = userEntityRepository.findByUsername(username).orElseThrow(EntityNotFoundException::new);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        if (userEntity == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(userEntity.getUsername(), userEntity.getPassword(), emptyList());
    }
}
