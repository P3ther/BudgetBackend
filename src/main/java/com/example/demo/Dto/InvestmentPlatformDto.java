package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvestmentPlatformDto {
    private Long id;
    private String name;
    private List<InvestmentDto> investmentsOnPlatform;
    private BigDecimal totalValue;
    private BigDecimal totalGain;
}
