package com.example.demo.Entity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
    List<CategoryEntity> findByUserWithAccessCategory_Username(String username);
    List<CategoryEntity> findAllByTypeAndUserWithAccessCategory_Username(Integer type, String username);
    Optional<CategoryEntity> findByIdAndUserWithAccessCategory_Username(Long id, String username);
}
