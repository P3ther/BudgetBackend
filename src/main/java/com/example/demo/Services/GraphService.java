package com.example.demo.Services;

import com.example.demo.Dto.GraphValueDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Entity.ExpenseEntity;
import com.example.demo.Entity.ExpenseRepository;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GraphService {
    private final ExpenseRepository expenseRepository;

    public GraphService(ExpenseRepository expenseRepository) {
        this.expenseRepository = expenseRepository;
    }

    public List<GraphValueDto> getExpensesGraphValues(StartEndTimeDto dateRange, String user) {
        List<GraphValueDto> graphValueDtos = new ArrayList<>();
        List<ExpenseEntity> expenseEntities = expenseRepository.findAllByDateBetweenAndInvestmentExpenseIsNullAndUserWithAccessExpense_Username(
            dateRange.getStartDateTime(),
            dateRange.getEndDateTime(),
            user
        );
        LocalDateTime tempDateTime = dateRange.getStartDateTime();
        while (tempDateTime.isBefore(dateRange.getEndDateTime())) {
            GraphValueDto graphValueDto = GraphValueDto.builder().build();
            graphValueDto.setDateTime(tempDateTime);
            LocalDateTime finalTempDateTime = tempDateTime;
            List<ExpenseEntity> tempExpenses = expenseEntities.stream().filter(
                    c -> c.getDate().isAfter(LocalDateTime.of(
                            finalTempDateTime.getYear(),
                            (finalTempDateTime.getMonth().getValue() + 1),
                            1,
                            0,
                            0,
                            0)
                    )
                    |
                    c.getDate().isBefore(LocalDateTime.of(
                            finalTempDateTime.getYear(),
                            (finalTempDateTime.getMonth().getValue() + 1),
                            finalTempDateTime.getMonth().length(finalTempDateTime.toLocalDate().isLeapYear()),
                            23,
                            59,
                            59)
                    )
            ).collect(Collectors.toList());
            BigDecimal tempValue = BigDecimal.valueOf(0L);
            for (ExpenseEntity entity: tempExpenses) {
                tempValue.add(entity.getPrice());
            }
            tempDateTime = tempDateTime.plusMonths(1L);
            graphValueDtos.add(
                    GraphValueDto.builder()
                            .value(tempValue)
                            .dateTime(
                                    LocalDateTime.of(
                                            tempDateTime.getYear(),
                                            tempDateTime.getMonthValue(),
                                            15,
                                            0,
                                            0,
                                            0
                                    )
                            )
                            .build()
            );
        }
        return graphValueDtos;
    }
}
