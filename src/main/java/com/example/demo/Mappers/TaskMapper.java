package com.example.demo.Mappers;

import com.example.demo.Dto.TaskDto;
import com.example.demo.Entity.TaskEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TaskMapper {
    private final UserEntityRepository userRepository;

    public TaskMapper(UserEntityRepository userRepository) {
        this.userRepository = userRepository;
    }

    public TaskDto convertToDto(TaskEntity taskEntity) {
        return TaskDto.builder()
                .id(taskEntity.getId())
                .taskName(taskEntity.getTaskName())
                .taskDetails(taskEntity.getTaskDetails())
                .dateCreated(taskEntity.getDateCreated())
                .dateExpired(taskEntity.getDateExpired())
                .priority(taskEntity.getPriority())
                .build();
    }
    public List<TaskDto> convertToListDto(List<TaskEntity> taskEntityList) {
        List<TaskDto> taskDtoList = new ArrayList<>();
        for (TaskEntity task: taskEntityList) {
            taskDtoList.add(convertToDto(task));
        }
        return taskDtoList;
    }
    public TaskEntity convertToEntity(TaskDto taskDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        return TaskEntity.builder()
                .id(taskDto.getId()/*taskDto.getId() == null ? -1 : taskDto.getId()*/)
                .taskName(taskDto.getTaskName())
                .taskDetails(taskDto.getTaskDetails())
                .dateCreated(taskDto.getDateCreated())
                .dateExpired(taskDto.getDateExpired())
                .priority(taskDto.getPriority())
                .userEntity(userEntity)
                .build();
    }
    public List<TaskEntity> convertToListEntity(List<TaskDto> taskDtoList, String user) throws EntityNotFoundException {
        List<TaskEntity> taskEntityList = new ArrayList<>();
        for (TaskDto task: taskDtoList) {
            taskEntityList.add(convertToEntity(task, user));
        }
        return taskEntityList;
    }
}
