package com.example.demo.Mappers;

import com.example.demo.Dto.ExpenseDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import com.example.demo.EntityRepositories.InvestmentEntityRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExpenseMapper {
    private final CategoryRepository categoryRepository;
    private final InvestmentEntityRepository investmentEntityRepository;
    private final CurrencyEntityRepository currencyEntityRepository;

    public ExpenseMapper(CategoryRepository categoryRepository, InvestmentEntityRepository investmentEntityRepository, CurrencyEntityRepository currencyEntityRepository) {
        this.categoryRepository = categoryRepository;
        this.investmentEntityRepository = investmentEntityRepository;
        this.currencyEntityRepository = currencyEntityRepository;
    }

    public ExpenseDto convertToDto(ExpenseEntity expenseEntity) {
        return ExpenseDto.builder()
                .id(expenseEntity.getId())
                .name(expenseEntity.getName())
                .date(expenseEntity.getDate())
                .price(expenseEntity.getPrice())
                .addressToReceipt(expenseEntity.getAddressToReceipt())
                .investmentId(expenseEntity.getInvestmentExpense() == null ? -1 : expenseEntity.getInvestmentExpense().getId())
                .category(expenseEntity.getCategoryExpense() == null ? -1 : expenseEntity.getCategoryExpense().getId())
                .categoryName(expenseEntity.getCategoryExpense() == null ? "" : expenseEntity.getCategoryExpense().getName())
                .currencyId(expenseEntity.getExpenseCurrency() == null ? -1 : expenseEntity.getExpenseCurrency().getId())
                .currencySign(expenseEntity.getExpenseCurrency() == null ? "" :expenseEntity.getExpenseCurrency().getSign())
                .build();
    }

    public ExpenseEntity convertToEntity(ExpenseDto expenseDto) {
        InvestmentEntity investmentEntity;
        CurrencyEntity currencyEntity;
        if(expenseDto.getInvestmentId() != null) {
            investmentEntity = investmentEntityRepository.findById(expenseDto.getInvestmentId()).orElse(null);
        } else {
            investmentEntity = null;
        }
        if(expenseDto.getCurrencyId() != null) {
            currencyEntity = currencyEntityRepository.findById(expenseDto.getCurrencyId()).orElse(null);
        } else {
            currencyEntity = null;
        }
        return ExpenseEntity.builder()
                .id(expenseDto.getId())
                .name(expenseDto.getName())
                .date(expenseDto.getDate())
                .price(expenseDto.getPrice())
                .addressToReceipt(expenseDto.getAddressToReceipt())
                .expenseCurrency(currencyEntity)
                .investmentExpense(investmentEntity)
                .categoryExpense(expenseDto.getCategory() == null ?
                        null
                        :
                        categoryRepository.findById(expenseDto.getCategory()).orElse(null))
                .build();
    }

    public List<ExpenseDto> convertToListDto(List<ExpenseEntity> expenseEntities) {
        List<ExpenseDto> expenseDtoList = new ArrayList<>();
        for (ExpenseEntity expenseEntity : expenseEntities) {
            expenseDtoList.add(convertToDto(expenseEntity));
        }
        return expenseDtoList;
    }
    public List<ExpenseEntity> convertToListEntity(List<ExpenseDto> expenseDtoList) {
        List<ExpenseEntity> expenseEntities = new ArrayList<>();
        for (ExpenseDto expenseDto: expenseDtoList) {
            expenseEntities.add(convertToEntity(expenseDto));
        }
        return  expenseEntities;
    }
}
