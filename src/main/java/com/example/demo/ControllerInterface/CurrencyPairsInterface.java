package com.example.demo.ControllerInterface;

import com.example.demo.Dto.CurrencyPairsDto;
import org.springframework.web.bind.annotation.GetMapping;

import java.text.ParseException;
import java.util.List;

public interface CurrencyPairsInterface {
    @GetMapping("/currencyPairs/")
    List<CurrencyPairsDto> getCurrencyPairs();

    @GetMapping("/exchangeRates/")
     void updateExchangeRates() throws ParseException;
}
