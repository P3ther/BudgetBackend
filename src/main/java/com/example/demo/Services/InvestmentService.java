package com.example.demo.Services;

import com.example.demo.Entity.InvestmentEntity;
import com.example.demo.EntityRepositories.InvestmentEntityRepository;
import org.springframework.stereotype.Service;

@Service
public class InvestmentService {
    private final InvestmentEntityRepository investmentEntityRepository;

    public InvestmentService(InvestmentEntityRepository investmentEntityRepository) {
        this.investmentEntityRepository = investmentEntityRepository;
    }

    public InvestmentEntity addInvestment(InvestmentEntity investmentEntity) {
        return  investmentEntityRepository.save(investmentEntity);
    }
}
