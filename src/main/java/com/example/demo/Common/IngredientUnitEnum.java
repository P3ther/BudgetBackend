package com.example.demo.Common;

public enum IngredientUnitEnum {
    Kilogram,
    Gram,
    Liter,
    Pcs,
    Mililiters,
}
