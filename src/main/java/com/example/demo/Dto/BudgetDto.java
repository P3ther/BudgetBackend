package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BudgetDto {
    private Long id;
    private List<ExpenseDto> expensesList;
    private List<IncomeDto> incomeList;
    private BigDecimal totalBalance;
    private BigDecimal totalExpense;
    private BigDecimal totalIncome;
    private BigDecimal expectedIncome;
    private BigDecimal expectedExpense;
    private BigDecimal totalInvestmentsValue;
    private BigDecimal totalInvestmentsGain;
}
