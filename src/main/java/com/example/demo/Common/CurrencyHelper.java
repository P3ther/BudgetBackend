package com.example.demo.Common;

import com.example.demo.Dto.CurrencyDto;
import com.example.demo.Entity.CurrencyPairsEntity;
import com.example.demo.Entity.UserEntity;
import com.example.demo.EntityRepositories.CurrencyPairsEntityRepository;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class CurrencyHelper {
    private final CurrencyPairsEntityRepository currencyPairsRepository;
    private final UserEntityRepository userRepository;

    public CurrencyHelper(CurrencyPairsEntityRepository currencyPairsRepository, UserEntityRepository userRepository) {
        this.currencyPairsRepository = currencyPairsRepository;
        this.userRepository = userRepository;
    }

    public double getExchangeRate(
            Long incomeExpenseId,
            List<CurrencyPairsEntity> currencyPairsEntityList,
            UserEntity userEntity
    ) {
        double exchangeRate = 0;
        for(CurrencyPairsEntity currencyPairsEntity : currencyPairsEntityList) {
            if(currencyPairsEntity.getFirstCurrency().getId().equals(
                    userEntity.getDefaultCurrency().getId())
                    & currencyPairsEntity.getSecondCurrency().getId().equals(incomeExpenseId)) {
                exchangeRate = 1 / currencyPairsEntity.getExchangeRate().doubleValue();
            } else if(currencyPairsEntity.getSecondCurrency().getId().equals(
                    userEntity.getDefaultCurrency().getId())
                    & currencyPairsEntity.getFirstCurrency().getId().equals(incomeExpenseId)) {
                exchangeRate = currencyPairsEntity.getExchangeRate().doubleValue();
            }
        }
        return exchangeRate;
    }
    public BigDecimal convertToDefaultCurrency(
            String username,
            BigDecimal value,
            CurrencyDto valueCurrency
    ) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username).orElseThrow(EntityNotFoundException::new);
        List<CurrencyPairsEntity> currencyPairsEntityList = currencyPairsRepository.findAll();
        if(!userEntity.getDefaultCurrency().getId().equals(valueCurrency.getId())) {
            double exchangeRate = 0;
            for(CurrencyPairsEntity currencyPairsEntity : currencyPairsEntityList) {
                if(currencyPairsEntity.getFirstCurrency().getId().equals(
                        userEntity.getDefaultCurrency().getId())
                        & currencyPairsEntity.getSecondCurrency().getId().equals(valueCurrency.getId())) {
                    exchangeRate = 1 / currencyPairsEntity.getExchangeRate().doubleValue();
                } else if(currencyPairsEntity.getSecondCurrency().getId().equals(
                        userEntity.getDefaultCurrency().getId())
                        & currencyPairsEntity.getFirstCurrency().getId().equals(valueCurrency.getId())) {
                    exchangeRate = currencyPairsEntity.getExchangeRate().doubleValue();
                }
            }
            return value.multiply(BigDecimal.valueOf(exchangeRate));
        } else {
            return value;
        }
    }
}
