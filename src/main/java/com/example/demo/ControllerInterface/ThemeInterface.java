package com.example.demo.ControllerInterface;

import com.example.demo.Dto.ThemeDto;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface ThemeInterface {

    @GetMapping("/themes/")
    public List<ThemeDto> getThemes();
}
