package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.Dto.*;
import com.example.demo.EndPoints.UserEndpoints;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Exceptions.ForbidenException;
import com.example.demo.Mappers.CurrencyMapper;
import com.example.demo.Mappers.ThemeMapper;
import com.example.demo.Mappers.UserMapper;
import com.example.demo.Security.SecurityConstants;
import com.example.demo.Services.UserService;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
public class User {
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserHelper userHelper;
    private final CurrencyMapper currencyMapper;
    private final ThemeMapper themeMapper;

    public User(UserService userService, UserMapper userMapper, UserHelper userHelper, CurrencyMapper currencyMapper, ThemeMapper themeMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.userHelper = userHelper;
        this.currencyMapper = currencyMapper;
        this.themeMapper = themeMapper;
    }

    @PostMapping(SecurityConstants.SIGN_UP_URL)
    public void signUp(@Valid @RequestBody UserDto userDto) throws Exception {
        userService.signUp(userMapper.convertToEntity(userDto));
    }

    @DeleteMapping("/user/delete/")
    public void deleteUser(@Valid @RequestBody UserDto userDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException {
       userService.deleteUser(userMapper.convertToEntity(userDto), userHelper.getUser(token));
    }

    @PostMapping("/user/changePassword/")
    public void changePassword(@Valid @RequestBody ChangePasswordDto changePasswordDto) throws EntityNotFoundException {
        userService.changePassword(changePasswordDto);
    }

    @PostMapping(UserEndpoints.forgotPassword)
    public void generateForgotPasswordCode(@RequestBody UserDto userDto) throws EntityNotFoundException, MessagingException {
        userService.generateForgotPasswordCode(userMapper.convertToEntity(userDto));
    }

    @PostMapping(UserEndpoints.verifyPasswordRegenerationCode)
    public void verifyPasswordRegenerationCode(@RequestBody UserDto userDto) throws Exception {
        userService.verifyPasswordRegenerationCode(userMapper.convertToEntity(userDto));
    }

    @PostMapping(UserEndpoints.resetPassword)
    public void resetPassword(@RequestBody UserDto userDto) throws ForbidenException, EntityNotFoundException {
        userService.resetPassword(userMapper.convertToEntity(userDto));
    }

    @GetMapping(UserEndpoints.userProperties)
    public UserDto getUserProperties(@RequestHeader("Authorization") String token) throws EntityNotFoundException {
        return userMapper.convertToDto(
                userService.getUserProperties(userHelper.getUser(token))
        );
    }

    @PostMapping(UserEndpoints.userProperties)
    public UserDto updateUserProperties(@RequestBody UserDto userDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException {
        return userMapper.convertToDto(
                userService.updateUserProperties(userMapper.convertToEntity(userDto))
        );
    }
}
