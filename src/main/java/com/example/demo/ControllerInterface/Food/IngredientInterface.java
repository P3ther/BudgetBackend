package com.example.demo.ControllerInterface.Food;

import com.example.demo.Dto.Food.IngredientDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.List;

public interface IngredientInterface {

    @GetMapping("/ingredient/{id}/")
    IngredientDto getIngredient(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/ingredient/")
    IngredientDto createIngredient(@RequestBody IngredientDto ingredientDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PutMapping("/ingredient/")
    IngredientDto updateIngredient(@RequestBody IngredientDto ingredientDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/ingredients/")
    List<IngredientDto> listIngredient(@RequestHeader("Authorization") String token);

    @DeleteMapping("/ingredient/{id}/")
    void deleteIngredient(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;
}