package com.example.demo.EntityRepositories;

import com.example.demo.Entity.ThemeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ThemeEntityRepository extends JpaRepository<ThemeEntity, Long> {

}
