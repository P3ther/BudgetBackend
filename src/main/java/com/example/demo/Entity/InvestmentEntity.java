package com.example.demo.Entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvestmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private LocalDateTime dateTime;
    private BigDecimal investment;
    // Should be one to one!
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "investmentExpense")
    private List<ExpenseEntity> investedExpense;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "investmentIncome")
    private  List<IncomeEntity> investmentIncome;
    private BigDecimal gain;
    @ManyToOne()
    @JoinColumn(name = "investmentCurrencyId")
    private CurrencyEntity investmentCurrency;
    @ManyToOne()
    @JoinColumn(name = "investmentPlatform")
    private InvestmentPlatformEntity investmentPlatform;
    @ManyToOne()
    @JoinColumn(name = "userWithAccessInvestment")
    private UserEntity userWithAccessInvestment;
}
