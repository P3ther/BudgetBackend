package com.example.demo.Services;

import com.example.demo.Entity.TaskEntity;
import com.example.demo.EntityRepositories.TaskEntityRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    private final TaskEntityRepository taskEntityRepository;

    public TaskService(TaskEntityRepository taskEntityRepository) {
        this.taskEntityRepository = taskEntityRepository;
    }

    public TaskEntity getTask(Long id, String user) throws EntityNotFoundException {
        return taskEntityRepository.findByIdAndUserEntity_Username(id, user).orElseThrow(EntityNotFoundException::new);
    }

    public TaskEntity createTask(TaskEntity taskEntity) {
        return taskEntityRepository.save(taskEntity);
    }

    public TaskEntity updateTask(TaskEntity taskEntity, String user) throws EntityNotFoundException {
        TaskEntity editedTask = taskEntityRepository.findByIdAndUserEntity_Username(taskEntity.getId(), user).orElseThrow(EntityNotFoundException::new);
        editedTask.setTaskName(taskEntity.getTaskName());
        editedTask.setTaskDetails(taskEntity.getTaskDetails());
        editedTask.setDateCreated(taskEntity.getDateCreated());
        editedTask.setDateExpired(taskEntity.getDateExpired());
        editedTask.setPriority(taskEntity.getPriority());
        return taskEntityRepository.save(editedTask);
    }

    public List<TaskEntity> listTasks(String user) {
        return taskEntityRepository.findAllByUserEntity_Username(user);
    }

    public void removeTask(Long id, String user) throws EntityNotFoundException {
        TaskEntity deletedEntity = taskEntityRepository.findByIdAndUserEntity_Username(id, user).orElseThrow(EntityNotFoundException::new);
        taskEntityRepository.delete(deletedEntity);
    }
}
