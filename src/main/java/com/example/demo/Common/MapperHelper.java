package com.example.demo.Common;

import org.springframework.stereotype.Component;

@Component
public class MapperHelper {

    public Long handleLongNull(Long value) {
        if (value == null) {
            return null;
        } else {
            return  value;
        }
    }
}
