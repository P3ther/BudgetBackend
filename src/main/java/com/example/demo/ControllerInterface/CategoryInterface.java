package com.example.demo.ControllerInterface;

import com.example.demo.Dto.CategoryDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface CategoryInterface {

    @GetMapping("/category/{id}")
    CategoryDto readCategory(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/category/{id}/time/")
    CategoryDto readCategoryByTime(@PathVariable("id") Long id, @RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/categories/")
    List<CategoryDto> getCategories(@RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/categories/time/")
    List<CategoryDto> getCategoriesAndValuesForTime(@RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/categories/expense/")
    List<CategoryDto> getExpenseCategories(@RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @GetMapping("/categories/income/")
    List<CategoryDto> getIncomeCategories(@RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PostMapping("/category/")
    CategoryDto addCategory(@RequestBody CategoryDto categoryDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @PutMapping("/category/")
    CategoryDto updateCategory(@RequestBody CategoryDto categoryDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @DeleteMapping("/category/{id}/")
    void deleteCategory(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

    @DeleteMapping("/category/{id}/all/")
    void deleteCategoryAndAllItems(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) throws EntityNotFoundException;

}
