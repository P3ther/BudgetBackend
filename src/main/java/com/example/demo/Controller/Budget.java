package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.Dto.BudgetDto;
import com.example.demo.Dto.StartEndTimeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Services.BudgetService;
import org.springframework.web.bind.annotation.*;

@RestController
public class Budget {
    private final BudgetService budgetService;
    private final UserHelper userHelper;

    public Budget(BudgetService budgetService, UserHelper userHelper) {
        this.budgetService = budgetService;
        this.userHelper = userHelper;
    }
    @GetMapping("budget/{year}/{month}")
    public BudgetDto getMonthlyBudget(@PathVariable("year")Integer year, @PathVariable("month") Integer month,  @RequestHeader("Authorization") String token) throws EntityNotFoundException {
        return budgetService.getMonthlyBudget(year, month, userHelper.getUser(token));
    }

    @PostMapping("budget/")
    public BudgetDto getTimedBudget(@RequestBody StartEndTimeDto startEndTimeDto, @RequestHeader("Authorization") String token) throws EntityNotFoundException {
        return budgetService.getTimedBudget(startEndTimeDto, userHelper.getUser(token));
    }
}
