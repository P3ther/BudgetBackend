package com.example.demo.Mappers;

import com.example.demo.Dto.InvestmentDto;
import com.example.demo.Entity.*;
import com.example.demo.EntityRepositories.UserEntityRepository;
import com.example.demo.EntityRepositories.CurrencyEntityRepository;
import com.example.demo.EntityRepositories.InvestmentPlatformRepository;
import com.example.demo.Exceptions.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class InvestmentMapper {
    private final InvestmentPlatformRepository investmentPlatformRepository;
    private final UserEntityRepository userRepository;
    private final CurrencyEntityRepository currencyEntityRepository;

    public InvestmentMapper(InvestmentPlatformRepository investmentPlatformRepository, UserEntityRepository userRepository, CurrencyEntityRepository currencyEntityRepository) {
        this.investmentPlatformRepository = investmentPlatformRepository;
        this.userRepository = userRepository;
        this.currencyEntityRepository = currencyEntityRepository;
    }

    public InvestmentDto convertToDto(InvestmentEntity investmentEntity) {
        return InvestmentDto.builder()
                .id(investmentEntity.getId())
                .dateTime(investmentEntity.getDateTime())
                .investment(investmentEntity.getInvestment())
                .gain(investmentEntity.getGain())
                .investmentPlatformId(investmentEntity.getInvestmentPlatform().getId())
                .investmentPlatformName(investmentEntity.getInvestmentPlatform().getName())
                .currencyId(investmentEntity.getInvestmentCurrency().getId())
                .currencySign(investmentEntity.getInvestmentCurrency().getSign())
                .build();
    }

    public InvestmentEntity convertToEntity(InvestmentDto investmentDto, String user) throws EntityNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(user).orElseThrow(EntityNotFoundException::new);
        InvestmentPlatformEntity investmentPlatform = investmentPlatformRepository.findByIdAndUserWithAccessInvestmentPlatform_Username(
                investmentDto.getInvestmentPlatformId(),
                user
        ).orElseThrow(EntityNotFoundException::new);
        CurrencyEntity currencyEntity = currencyEntityRepository.findById(investmentDto.getCurrencyId()).orElse(null);
        return InvestmentEntity.builder()
                .id(investmentDto.getId() == null? -1 : investmentDto.getId())
                .dateTime(investmentDto.getDateTime())
                .investment(investmentDto.getInvestment())
                .gain(investmentDto.getGain())
                .investmentCurrency(currencyEntity)
                .investmentPlatform(investmentPlatform)
                .userWithAccessInvestment(userEntity)
                .build();
    }
    public List<InvestmentDto> convertToListDto(List<InvestmentEntity> investmentEntities) {
        List<InvestmentDto> investmentDtoList = new ArrayList<>();
        for (InvestmentEntity investmentEntity : investmentEntities) {
            investmentDtoList.add(convertToDto(investmentEntity));
        }
        return  investmentDtoList;
    }

    public List<InvestmentEntity> convertToListEntity(List<InvestmentDto> investmentDtoList, String user) throws EntityNotFoundException {
        List<InvestmentEntity> investmentEntities = new ArrayList<>();
        for (InvestmentDto investmentDto: investmentDtoList) {
            investmentEntities.add(convertToEntity(investmentDto, user));
        }
        return  investmentEntities;
    }
}
