package com.example.demo.Common;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.example.demo.Security.SecurityConstants.*;

@Component
public class UserHelper {
    public String getUser(String token) {
        String user = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();
        return user;
    }
}
