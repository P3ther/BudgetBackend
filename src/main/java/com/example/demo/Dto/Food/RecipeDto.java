package com.example.demo.Dto.Food;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RecipeDto {
    private Long id;
    @NotBlank(message = "Recipe has to have name")
    private String recipeName;
    @NotBlank(message = "Recipe has to have instructions")
    private String recipeInstructions;
    private List<RecipeIngredientsDtoRecipe> recipeIngredients;
    private LocalDateTime lastDateTimeUsed;
    private Long recipeCalories;
}
