package com.example.demo.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThemeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true, nullable = false)
    private String themeName;
    @Column(unique = true, nullable = false)
    private String themeFilename;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "defaultTheme")
    private List<UserEntity> usersDefaultThemes;
}
