package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.InvestmentPlatformInterface;
import com.example.demo.Dto.InvestmentPlatformDto;
import com.example.demo.Entity.InvestmentPlatformEntity;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.InvestmentPlatformMapper;
import com.example.demo.Services.InvestmentPlatformService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
public class InvestmentPlatform implements InvestmentPlatformInterface {
    private final InvestmentPlatformService investmentPlatformService;
    private final InvestmentPlatformMapper investmentPlatformMapper;
    private final UserHelper userHelper;

    public InvestmentPlatform(InvestmentPlatformService investmentPlatformService, InvestmentPlatformMapper investmentPlatformMapper, UserHelper userHelper) {
        this.investmentPlatformService = investmentPlatformService;
        this.investmentPlatformMapper = investmentPlatformMapper;
        this.userHelper = userHelper;
    }

    public InvestmentPlatformDto getInvestmentPlatform(Long id, String token) throws EntityNotFoundException {
        return investmentPlatformMapper.convertToDto(investmentPlatformService.getInvestmentPlatform(id, userHelper.getUser(token)));
    }

    public InvestmentPlatformDto createInvestmentPlatform(InvestmentPlatformDto investmentPlatformDto, String token) throws EntityNotFoundException {
        return investmentPlatformMapper.convertToDto(
                investmentPlatformService.createInvestmentPlatform(
                        investmentPlatformMapper.convertToEntity(investmentPlatformDto, userHelper.getUser(token))
                )
        );
    }

    public InvestmentPlatformDto updateInvestmentPlatform(InvestmentPlatformDto investmentPlatformDto, String token) throws EntityNotFoundException {
        return investmentPlatformMapper.convertToDto(
                investmentPlatformService.updateInvestmentPlatform(
                        investmentPlatformMapper.convertToEntity(investmentPlatformDto, userHelper.getUser(token))
                )
        );
    }

    public void deleteInvestmentPlatform(Long id, String token) throws EntityNotFoundException {
        investmentPlatformService.deleteInvestmentPlatform(id, userHelper.getUser(token));
    }

    public List<InvestmentPlatformDto> listInvestmentPlatforms(String token) {
        return investmentPlatformMapper.convertToListDto(
                investmentPlatformService.listInvestmentPlatforms(userHelper.getUser(token))
        );
    }
}
