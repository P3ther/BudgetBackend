package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskDto {
    private Long id;
    private String taskName;
    private String taskDetails;
    private LocalDateTime dateCreated;
    private LocalDateTime dateExpired;
    private Integer priority;
}
