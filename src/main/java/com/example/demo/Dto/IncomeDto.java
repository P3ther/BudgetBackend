package com.example.demo.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IncomeDto {
    private Long id;
    private String name;
    private LocalDateTime date;
    private BigDecimal price;
    private String addressToReceipt;
    private Long investmentId;
    private Long category;
    private String categoryName;
    private Long currencyId;
    private String currencySign;
}
