# BudgetBackend

Deploy:
1.  sudo chmod +x gradlew 
2.  /gradlew build
3.  docker build --build-arg JAR_FILE=build/libs/\*.jar -t budget/budget-backend:1.0.0 .
4.  docker run -d -p 8085:8080 --network=budget --name=budget-backend -p 8445:443 budget/budget-backend:1.0.0
