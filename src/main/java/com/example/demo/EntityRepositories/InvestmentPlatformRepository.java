package com.example.demo.EntityRepositories;

import com.example.demo.Entity.InvestmentPlatformEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface InvestmentPlatformRepository extends JpaRepository<InvestmentPlatformEntity, Long>  {
    Optional<InvestmentPlatformEntity> findByIdAndUserWithAccessInvestmentPlatform_Username(Long Id, String userName);
    List<InvestmentPlatformEntity> findAllByUserWithAccessInvestmentPlatform_Username(String userName);

}
