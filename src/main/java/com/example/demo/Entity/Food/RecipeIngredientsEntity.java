package com.example.demo.Entity.Food;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecipeIngredientsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne()
    @PrimaryKeyJoinColumn(name = "recipe_id")
    private RecipeEntity recipe;
    @ManyToOne()
    @PrimaryKeyJoinColumn(name = "ingredient_id")
    private IngredientEntity ingredient;
    private BigDecimal quantity;
}
