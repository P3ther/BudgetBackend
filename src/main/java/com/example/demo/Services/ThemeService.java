package com.example.demo.Services;

import com.example.demo.Entity.ThemeEntity;
import com.example.demo.EntityRepositories.ThemeEntityRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThemeService {
    private final ThemeEntityRepository themeEntityRepository;

    public ThemeService(ThemeEntityRepository themeEntityRepository) {
        this.themeEntityRepository = themeEntityRepository;
    }

    public List<ThemeEntity> getThemes() {
        return themeEntityRepository.findAll();
    }
}
