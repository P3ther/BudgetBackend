package com.example.demo.Controller;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.InvestmentInterface;
import com.example.demo.Dto.InvestmentDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.InvestmentMapper;
import com.example.demo.Services.InvestmentService;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Investment implements InvestmentInterface {
    private final InvestmentService investmentService;
    private final UserHelper userHelper;
    private final InvestmentMapper investmentMapper;

    public Investment(InvestmentService investmentService, UserHelper userHelper, InvestmentMapper investmentMapper) {
        this.investmentService = investmentService;
        this.userHelper = userHelper;
        this.investmentMapper = investmentMapper;
    }

    public InvestmentDto addInvestment(InvestmentDto investmentDto, String token) throws EntityNotFoundException {
        return investmentMapper.convertToDto(
                investmentService.addInvestment(
                        investmentMapper.convertToEntity(investmentDto, userHelper.getUser(token))
                )
        );
    }
}
