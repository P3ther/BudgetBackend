package com.example.demo.Controller.Food;

import com.example.demo.Common.UserHelper;
import com.example.demo.ControllerInterface.Food.RecipeInterface;
import com.example.demo.Dto.Food.RecipeDto;
import com.example.demo.Exceptions.EntityNotFoundException;
import com.example.demo.Mappers.Food.RecipeMapper;
import com.example.demo.Services.Food.RecipeService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Recipe implements RecipeInterface {
    private final RecipeMapper recipeMapper;
    private final RecipeService recipeService;
    private final UserHelper userHelper;

    public Recipe(RecipeMapper recipeMapper, RecipeService recipeService, UserHelper userHelper) {
        this.recipeMapper = recipeMapper;
        this.recipeService = recipeService;
        this.userHelper = userHelper;
    }

    public RecipeDto getRecipe(Long id, String token) throws EntityNotFoundException {
        return recipeMapper.convertToDto(
                recipeService.getRecipe(id, userHelper.getUser(token))
        );
    }
    public RecipeDto createRecipe(RecipeDto recipeDto, String token) throws EntityNotFoundException {
        return recipeMapper.convertToDto(
                recipeService.createRecipe(
                        recipeMapper.convertToEntity(recipeDto, userHelper.getUser(token))
                )
        );
    }

    public RecipeDto updateRecipe(RecipeDto recipeDto, String token) throws EntityNotFoundException {
        return recipeMapper.convertToDto(
                recipeService.updateRecipe(
                        recipeMapper.convertToEntity(recipeDto, userHelper.getUser(token))
                )
        );
    }

    public List<RecipeDto> listRecipes(String token) {
        return recipeMapper.convertToListDto(
                recipeService.listRecipes(userHelper.getUser(token))
        );
    }

    public void deleteRecipe(Long id, String token) throws EntityNotFoundException {
        recipeService.deleteRecipe(id, userHelper.getUser(token));
    }
}
